﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
	DataSet ds = new DataSet();
	SqlDataAdapter da;
	string Conexion = ConfigurationManager.ConnectionStrings["ConexionSql"].ToString();
	public DataSet AGREGAR_USUARIOS (string USUARIO, string CLAVE, int ID_EMPLEADO, int ID_ROLL)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_USUARIOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@USUARIO", USUARIO);
			da.SelectCommand.Parameters.AddWithValue("@CLAVE", CLAVE);
			da.SelectCommand.Parameters.AddWithValue("@ID_EMPLEADO", ID_EMPLEADO);
			da.SelectCommand.Parameters.AddWithValue("@ID_ROL", ID_ROLL);

			da.Fill(ds, "Usuario Agregado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet MODIFICAR_USUARIOS(int ID_USUARIO, string USUARIO, string CLAVE, int ID_EMPLEADO, int ID_ROL)
	{
		try
		{
			da = new SqlDataAdapter("MODIFICAR_USUARIOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_USUARIO", ID_USUARIO);
			da.SelectCommand.Parameters.AddWithValue("@USUARIO", USUARIO);
			da.SelectCommand.Parameters.AddWithValue("@CLAVE", CLAVE);
			da.SelectCommand.Parameters.AddWithValue("@ID_EMPLEADO", ID_EMPLEADO);
			da.SelectCommand.Parameters.AddWithValue("@ID_ROL", ID_ROL);

			da.Fill(ds, "Usuario Modificado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet AGREGAR_CLIENTES(string NOMBRE, string APELLIDO, string DUI, string NIT, string DIRECCION, string TELEFONO, string EMAIL)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_CLIENTES", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE_CLIENTE", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@APELLIDO_CLIENTE", APELLIDO);			
			da.SelectCommand.Parameters.AddWithValue("@DUI", DUI);
			da.SelectCommand.Parameters.AddWithValue("@NIT", NIT);
			da.SelectCommand.Parameters.AddWithValue("@DIRECCION", DIRECCION);
			da.SelectCommand.Parameters.AddWithValue("@TELEFONO", TELEFONO);
			da.SelectCommand.Parameters.AddWithValue("@E_MAIL", EMAIL);

			da.Fill(ds, "Cliente Agregado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet MODIFICAR_CLIENTES(int ID_CLIENTE, string NOMBRE, string APELLIDO, string DUI, string NIT, string DIRECCION, string TELEFONO, string EMAIL)
	{
		try
		{
			da = new SqlDataAdapter("MODIFICAR_CLIENTES", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_CLIENTE", ID_CLIENTE);		
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE_CLIENTE", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@APELLIDO_CLIENTE", APELLIDO);
			da.SelectCommand.Parameters.AddWithValue("@DUI", DUI);
			da.SelectCommand.Parameters.AddWithValue("@NIT", NIT);
			da.SelectCommand.Parameters.AddWithValue("@DIRECCION", DIRECCION);
			da.SelectCommand.Parameters.AddWithValue("@TELEFONO", TELEFONO);
			da.SelectCommand.Parameters.AddWithValue("@E_MAIL", EMAIL);

			da.Fill(ds, "Cliente Modificado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet AGREGAR_EMPLEADOS(string NOMBRE, string APELLIDO, string DIRECCION, string TELEFONO, string EMAIL, string DUI, string NIT, double SALARIO)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_EMPLEADOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE_EMPLEADO", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@APELLIDO_EMPLEADO", APELLIDO);
			da.SelectCommand.Parameters.AddWithValue("@DIRECCION", DIRECCION);
			da.SelectCommand.Parameters.AddWithValue("@TELEFONO", TELEFONO);
			da.SelectCommand.Parameters.AddWithValue("@E_MAIL", EMAIL);
			da.SelectCommand.Parameters.AddWithValue("@DUI", DUI);
			da.SelectCommand.Parameters.AddWithValue("@NIT", NIT);
			da.SelectCommand.Parameters.AddWithValue("@SALARIO", SALARIO);

			da.Fill(ds, "Empleado Agregado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet MODIFICAR_EMPLEADOS(int ID_EMPLEADO, string NOMBRE, string APELLIDO, string DIRECCION, string TELEFONO, string EMAIL, string DUI, string NIT, double SALARIO)
	{
		try
		{
			da = new SqlDataAdapter("MODIFICAR_EMPLEADOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_EMPLEADO", ID_EMPLEADO);
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE_EMPLEADO", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@APELLIDO_EMPLEADO", APELLIDO);
			da.SelectCommand.Parameters.AddWithValue("@DIRECCION", DIRECCION);
			da.SelectCommand.Parameters.AddWithValue("@TELEFONO", TELEFONO);
			da.SelectCommand.Parameters.AddWithValue("@E_MAIL", EMAIL);
			da.SelectCommand.Parameters.AddWithValue("@DUI", DUI);
			da.SelectCommand.Parameters.AddWithValue("@NIT", NIT);
			da.SelectCommand.Parameters.AddWithValue("@SALARIO", SALARIO);	

			da.Fill(ds, "Empleado Modificado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}


	public DataSet AGREGAR_REPUESTOS(string NOMBRE, double PRECIO, int EXISTENCIAS)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_REPUESTOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE_REPUESTO", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@PRECIO", PRECIO);
			da.SelectCommand.Parameters.AddWithValue("@EXISTENCIAS", EXISTENCIAS);
		
			da.Fill(ds, "Repuesto Agregado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet MODIFICAR_REPUESTOS(int ID_REPUESTO, string NOMBRE, double PRECIO, int EXISTENCIAS)
	{
		try
		{
			da = new SqlDataAdapter("MODIFICAR_REPUESTOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_REPUESTO", ID_REPUESTO);
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE_REPUESTO", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@PRECIO", PRECIO);
			da.SelectCommand.Parameters.AddWithValue("@EXISTENCIAS", EXISTENCIAS);

			da.Fill(ds, "Repuesto Modificado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet AGREGAR_COTIZACION(string NOMBRE, string APELLIDO, string MARCA, string MODELO, int ANIO,DateTime FECHA, String SERVICIO,int CANTIDAD, String REPUESTO, double PRECIOU, double PRECIOT, double TOTAL)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_COTIZACION", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@APELLIDO", APELLIDO);
			da.SelectCommand.Parameters.AddWithValue("@MARCA", MARCA); 
			da.SelectCommand.Parameters.AddWithValue("@MODELO", MODELO);
			da.SelectCommand.Parameters.AddWithValue("@ANIO", ANIO);
			da.SelectCommand.Parameters.AddWithValue("@FECHA", FECHA);
			da.SelectCommand.Parameters.AddWithValue("@SERVICIO", SERVICIO); 
			da.SelectCommand.Parameters.AddWithValue("@CANTIDAD", CANTIDAD);
			da.SelectCommand.Parameters.AddWithValue("@REPUESTO", REPUESTO);
			da.SelectCommand.Parameters.AddWithValue("@PRECIO_UNITARIO", PRECIOU);
			da.SelectCommand.Parameters.AddWithValue("@PRECIO_TOTAL", PRECIOT);
			da.SelectCommand.Parameters.AddWithValue("@TOTAL_PAGO", TOTAL);

			da.Fill(ds, "Cotizacion Agregada");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet MODIFICA_COTIZACION(int ID_COTIZACION, string NOMBRE, string APELLIDO, string MARCA, string MODELO, int ANIO, DateTime FECHA, String SERVICIO, int CANTIDAD, String REPUESTO, double PRECIOU, double PRECIOT, double TOTAL)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_COTIZACION", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_COTIZACION", ID_COTIZACION);
			da.SelectCommand.Parameters.AddWithValue("@NOMBRE", NOMBRE);
			da.SelectCommand.Parameters.AddWithValue("@APELLIDO", APELLIDO);
			da.SelectCommand.Parameters.AddWithValue("@MARCA", MARCA);
			da.SelectCommand.Parameters.AddWithValue("@MODELO", MODELO);
			da.SelectCommand.Parameters.AddWithValue("@ANIO", ANIO);
			da.SelectCommand.Parameters.AddWithValue("@FECHA", FECHA);
			da.SelectCommand.Parameters.AddWithValue("@SERVICIO", SERVICIO);
			da.SelectCommand.Parameters.AddWithValue("@CANTIDAD", CANTIDAD);
			da.SelectCommand.Parameters.AddWithValue("@REPUESTO", REPUESTO);
			da.SelectCommand.Parameters.AddWithValue("@PRECIO_UNITARIO", PRECIOU);
			da.SelectCommand.Parameters.AddWithValue("@PRECIO_TOTAL", PRECIOT);
			da.SelectCommand.Parameters.AddWithValue("@TOTAL_PAGO", TOTAL);

			da.Fill(ds, "Cotizacion Agregada");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet AGREGAR_VEHICULOS(string PLACA, int ANIO, string VIN, string COLOR, int ID_MARCA, int ID_MODELO,  int ID_CLIENTE)
	{
		try
		{
			da = new SqlDataAdapter("AGREGAR_VEHICULOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Placa", PLACA);
			da.SelectCommand.Parameters.AddWithValue("@AÑO", ANIO);
			da.SelectCommand.Parameters.AddWithValue("@VIN", VIN);
			da.SelectCommand.Parameters.AddWithValue("@COLOR", COLOR);
			da.SelectCommand.Parameters.AddWithValue("@id_marca", ID_MARCA);
			da.SelectCommand.Parameters.AddWithValue("@ID_MODELOS", ID_MODELO);
			da.SelectCommand.Parameters.AddWithValue("@ID_CLIENTE", ID_CLIENTE);
			
			da.Fill(ds, "Vehiculo Agregado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}
	public DataSet MODIFICAR_VEHICULOS(int ID_VEHICULO,string PLACA, int ANIO, string VIN, string COLOR, int ID_MARCA, int ID_MODELO, int ID_CLIENTE)
	{
		try
		{
			da = new SqlDataAdapter("MODIFICAR_VEHICULOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_VEHICULO", ID_VEHICULO);
			da.SelectCommand.Parameters.AddWithValue("@placa", PLACA);
			da.SelectCommand.Parameters.AddWithValue("@Anio", ANIO);
			da.SelectCommand.Parameters.AddWithValue("@VIN", VIN);
			da.SelectCommand.Parameters.AddWithValue("@COLOR", COLOR);
			da.SelectCommand.Parameters.AddWithValue("@id_marca", ID_MARCA);
			da.SelectCommand.Parameters.AddWithValue("@ID_MODELO", ID_MODELO);
			da.SelectCommand.Parameters.AddWithValue("@ID_CLIENTE", ID_CLIENTE);

			da.Fill(ds, "Vehiculo Modificado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BUSCAR_VEHICULOS(int AÑO, string VIN, string COLOR, int ID_CLIENTE, int ID_MODELO)
	{
		try
		{
			da = new SqlDataAdapter("BUSCAR_VEHICULOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@AÑO", AÑO);
			da.SelectCommand.Parameters.AddWithValue("@VIN", VIN);
			da.SelectCommand.Parameters.AddWithValue("@COLOR", COLOR);
			da.SelectCommand.Parameters.AddWithValue("@ID_CLIENTE", ID_CLIENTE); 
			da.SelectCommand.Parameters.AddWithValue("@ID_MODELOS", ID_MODELO);

			da.Fill(ds, "Registros encontrados de vehiculos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BuscarCliente(string Buscar)
	{
		try
		{
			da = new SqlDataAdapter("BuscarCliente", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Buscar", Buscar);

			da.Fill(ds, "Registros encontrados de Clientes");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BuscarEmpleado(string Buscar)
	{
		try
		{
			da = new SqlDataAdapter("BuscarEmpleado", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Buscar", Buscar);

			da.Fill(ds, "Registros encontrados de Empleados");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BuscarRepuestos(string Buscar)
	{
		try
		{
			da = new SqlDataAdapter("BuscarRepuestos", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Buscar", Buscar);

			da.Fill(ds, "Registros encontrados de Repuestos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BuscarServicio(string Buscar)
	{
		try
		{
			da = new SqlDataAdapter("BuscarServicio", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Buscar", Buscar);

			da.Fill(ds, "Registros encontrados de Servicios");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BuscarVehiculo(string PLACA)
	{
		try
		{
			da = new SqlDataAdapter("BuscarVehiculo", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@PLACA", PLACA);

			da.Fill(ds, "Registros encontrados de Vehiculos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BusquedaEmpleado()
	{
		try
		{
			da = new SqlDataAdapter("BusquedaEmpleado", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;		

			da.Fill(ds, "Registros encontrados de Empleados");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet Busqueda_Estados()
	{
		try
		{
			da = new SqlDataAdapter("Busqueda_Estados", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Registros encontrados de Estados");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BusquedaRoles()
	{
		try
		{
			da = new SqlDataAdapter("BusquedaRoles", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;			

			da.Fill(ds, "Registros encontrados de Roles");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ValidarLogin(string USUARIO, string CLAVE)
	{
		try
		{
			da = new SqlDataAdapter("SP_ValidarLogin", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Usuario", USUARIO);
			da.SelectCommand.Parameters.AddWithValue("@CLAVE", CLAVE);
			
			da.Fill(ds, "Usuario encontrado exitosamente");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ELIMINAR_VEHICULOS(int ID_VEHICULO)
	{
		try
		{
			da = new SqlDataAdapter("ELIMINAR_VEHICULOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_VEHICULO", ID_VEHICULO);			

			da.Fill(ds, "Vehiculo Eliminado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet LLENADO_ORDENES()
	{
		try
		{
			da = new SqlDataAdapter("LLENADO_ORDENES", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Orden llenada");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ConsOrdenes(int ID_EMPLEADO)
	{
		try
		{
			da = new SqlDataAdapter("ConsOrdenes", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("ID_EMPLEADO", ID_EMPLEADO);

			da.Fill(ds, "Resgistro de Ordenes");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet DetalleOrden(int ID_ORDEN, int ID_SERVICIO, double CANTIDAD,int ID_REPUESTO)
	{
		try
		{
			da = new SqlDataAdapter("DetalleOrden", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;			
			da.SelectCommand.Parameters.AddWithValue("@Id_Orden", ID_ORDEN);
			da.SelectCommand.Parameters.AddWithValue("@Id_Servicio", ID_SERVICIO);
			da.SelectCommand.Parameters.AddWithValue("@Cantidad", CANTIDAD);
			da.SelectCommand.Parameters.AddWithValue("@Id_Repuesto", ID_REPUESTO);

			da.Fill(ds, "Detalle de Orden Agregada con exito");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet DetOrdenes()
	{
		try
		{
			da = new SqlDataAdapter("DetOrdenes", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Detalle de Orden");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet Genera_Factura(int ID_ORDEN,int ID_CLIENTE)
	{
		try
		{
			da = new SqlDataAdapter("Genera_Factura", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@id_orden", ID_ORDEN);
			da.SelectCommand.Parameters.AddWithValue("@ID_Cliente", ID_CLIENTE);
			
			da.Fill(ds, "Factura Generada con exito");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ImprimirFactura(int ID_ORDEN, DateTime FECHA)
	{
		try
		{
			da = new SqlDataAdapter("ImprimirFactura", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@id_orden", ID_ORDEN);
			da.SelectCommand.Parameters.AddWithValue("@fecha", FECHA);

			da.Fill(ds, "Factura Impresa con exito");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet Registro_de_Ordenes(int ID_CLIENTE, int ID_VEHICULO,DateTime FECHA,int ID_COTIZACION,int ID_EMPLEADO)
	{
		try
		{
			da = new SqlDataAdapter("Registro_de_Ordenes", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@id_cliente", ID_CLIENTE);
			da.SelectCommand.Parameters.AddWithValue("@ID_Vehiculo", ID_VEHICULO);
			da.SelectCommand.Parameters.AddWithValue("@fecha", FECHA);
			da.SelectCommand.Parameters.AddWithValue("@ID_Cotizacion", ID_COTIZACION);
			da.SelectCommand.Parameters.AddWithValue("@ID_Empleado", ID_EMPLEADO);

			da.Fill(ds, "Orden agregada con exito");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ReporteClientes()
	{
		try
		{
			da = new SqlDataAdapter("ReporteClientes", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Reporte Clientes");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ReporteEmpleados()
	{
		try
		{
			da = new SqlDataAdapter("ReporteEmpleados", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Reporte Empleados");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ReporteVehiculos()
	{
		try
		{
			da = new SqlDataAdapter("ReporteVehiculos", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Reporte Vehiculos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ReporteFacturas( DateTime FECHA)
	{
		try
		{
			da = new SqlDataAdapter("ReporteFacturas", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;			
			da.SelectCommand.Parameters.AddWithValue("@FECHA", FECHA);
			da.Fill(ds, "Reporte Facturas");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}
	 
	public DataSet ListarVehiculos(int idcliente)
	{
		try
		{
			da = new SqlDataAdapter("ListarVehiculos", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Idcliente", idcliente);

			da.Fill(ds, "Lista Vehiculos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet LlenadoDetalle()
	{
		try
		{
			da = new SqlDataAdapter("LlenadoDetalle", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "Detalle Llenado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet Consultarorden(int idOrden)
	{
		try
		{
			da = new SqlDataAdapter("Consultarorden", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@IdOrden", idOrden);

			da.Fill(ds, "Detalle Llenado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BUSCAR_MODELO(int ID_MARCA)
	{
		try
		{
			da = new SqlDataAdapter("BUSCAR_MODELO", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_MARCA", ID_MARCA);

			da.Fill(ds, "Detalle Llenado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BUSCAR_MARCA()
	{
		try
		{
			da = new SqlDataAdapter("BUSCAR_MARCA", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			

			da.Fill(ds, "Detalle Llenado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet BuscarCotizacion(int ID_COTIZACION)
	{
		try
		{
			da = new SqlDataAdapter("BuscarCotizacion", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Buscar", ID_COTIZACION);

			da.Fill(ds, "Cotizaciones encontradas");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet LlenadoFactura()
	{
		try
		{
			da = new SqlDataAdapter("LlenadoFactura", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			

			da.Fill(ds, "Llenado de Factura");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}
	public DataSet LlenadoFactura_Cliente(int ID_Orden)
	{
		try
		{
			da = new SqlDataAdapter("LlenadoFactura_Cliente", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_ORDEN", ID_Orden);

			da.Fill(ds, "llenada factura cliente");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet Listar_Repuesto()
	{
		try
		{
			da = new SqlDataAdapter("Listar_Repuesto", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;

			da.Fill(ds, "repuesto listado ");
			return ds;
		}
		catch (Exception ex)
		{
			return null;
		}
	}

	public DataSet ConsOrdenes_Detalle(int ID_EMPLEADO, int ID_ORDEN)
	{
		try
		{
			da = new SqlDataAdapter("ConsOrdenes_Detalle", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_EMPLEADO", ID_EMPLEADO);
			da.SelectCommand.Parameters.AddWithValue("@ID_ORDEN", ID_ORDEN);

			da.Fill(ds, "Consulta detalle");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ActualizarEstadoOrden(int ESTADO, int ID_ORDEN)
	{
		try
		{
			da = new SqlDataAdapter("ActualizarEstadoOrden", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@Estado", ESTADO);
			da.SelectCommand.Parameters.AddWithValue("@ID_ORDEN", ID_ORDEN);

			da.Fill(ds, "Estado modificado ");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ListarCotizaciones()
	{
		try
		{
			da = new SqlDataAdapter("ListarCotizaciones", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.Fill(ds, "listar cotizaciones");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ListarUsuarios()
	{
		try
		{
			da = new SqlDataAdapter("ListarUsuarios", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.Fill(ds, "listar Usuarios");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ListarEmpleados()
	{
		try
		{
			da = new SqlDataAdapter("ListarEmpleados", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.Fill(ds, "listar Empleados");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ListarClientes()
	{
		try
		{
			da = new SqlDataAdapter("ListarClientes", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.Fill(ds, "listar clientes");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ListarTablaREPUESTOS()
	{
		try
		{
			da = new SqlDataAdapter("ListarTablaREPUESTOS", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.Fill(ds, "listar repuestos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet ListarTablaVehiculos()
	{
		try
		{
			da = new SqlDataAdapter("ListarTablaVehiculos", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.Fill(ds, "listar vehiculos");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet EliminarDetalleOrden(int ID_DETALLE)
	{
		try
		{
			da = new SqlDataAdapter("EliminarDetalleOrden", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@IdDetalle", ID_DETALLE);
			da.Fill(ds, "Detalle Eliminado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

	public DataSet MODIFICAR_DetalleOrden(int ID_DETALLE,int ID_ORDEN,int ID_SERVICIO,int ID_REPUESTO, int Cantidad)
	{
		try
		{
			da = new SqlDataAdapter("MODIFICAR_DetalleOrden", Conexion);
			da.SelectCommand.CommandType = CommandType.StoredProcedure;
			da.SelectCommand.Parameters.AddWithValue("@ID_Detalle", ID_DETALLE);
			da.SelectCommand.Parameters.AddWithValue("@ID_Orden", ID_ORDEN);
			da.SelectCommand.Parameters.AddWithValue("@ID_Servicio", ID_SERVICIO);
			da.SelectCommand.Parameters.AddWithValue("@ID_Repuesto", ID_REPUESTO);
			da.SelectCommand.Parameters.AddWithValue("@Cantidad", Cantidad);
			da.Fill(ds, "Detalle Eliminado");
			return ds;
		}
		catch (Exception ex)
		{

			return null;
		}
	}

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract]
public interface IService
{

	[OperationContract]
	DataSet AGREGAR_USUARIOS(string USUARIO, string CLAVE, int ID_EMPLEADO, int ID_ROLL);

	[OperationContract]
	DataSet MODIFICAR_USUARIOS(int ID_USUARIO, string USUARIO, string CLAVE, int ID_EMPLEADO, int ID_ROL);

	[OperationContract]
	DataSet AGREGAR_CLIENTES(string NOMBRE, string APELLIDO, string DUI, string NIT, string DIRECCION, string TELEFONO, string EMAIL);

	[OperationContract]
	DataSet MODIFICAR_CLIENTES(int ID_CLIENTE, string NOMBRE, string APELLIDO, string DUI, string NIT, string DIRECCION, string TELEFONO, string EMAIL);

	[OperationContract]
	DataSet AGREGAR_REPUESTOS(string NOMBRE, double PRECIO, int EXISTENCIAS);

	[OperationContract]
	DataSet MODIFICAR_REPUESTOS(int ID_REPUESTO, string NOMBRE, double PRECIO, int EXISTENCIAS);

	[OperationContract]
	DataSet AGREGAR_EMPLEADOS(string NOMBRE, string APELLIDO, string DIRECCION, string TELEFONO, string EMAIL, string DUI, string NIT, double SALARIO);

	[OperationContract]
	DataSet MODIFICAR_EMPLEADOS(int ID_EMPLEADO, string NOMBRE, string APELLIDO, string DIRECCION, string TELEFONO, string EMAIL, string DUI, string NIT, double SALARIO);

	[OperationContract]
	DataSet AGREGAR_COTIZACION(string NOMBRE, string APELLIDO, string MARCA, string MODELO, int ANIO, DateTime FECHA, String SERVICIO, int CANTIDAD, String REPUESTO, double PRECIOU, double PRECIOT, double TOTAL);

	[OperationContract]
	DataSet MODIFICA_COTIZACION(int ID_COTIZACION, string NOMBRE, string APELLIDO, string MARCA, string MODELO, int ANIO, DateTime FECHA, String SERVICIO, int CANTIDAD, String REPUESTO, double PRECIOU, double PRECIOT, double TOTAL);

	[OperationContract]
	DataSet AGREGAR_VEHICULOS(string PLACA, int ANIO, string VIN, string COLOR, int ID_MARCA, int ID_MODELO, int ID_CLIENTE);


	[OperationContract]
	DataSet MODIFICAR_VEHICULOS(int ID_VEHICULO, string PLACA, int ANIO, string VIN, string COLOR, int ID_MARCA, int ID_MODELO, int ID_CLIENTE);

	[OperationContract]
	DataSet BUSCAR_VEHICULOS(int AÑO, string VIN, string COLOR, int ID_CLIENTE, int ID_MODELO);
	
	[OperationContract]
	DataSet BuscarCliente(string Buscar);
	
	[OperationContract]
	DataSet BuscarEmpleado(string Buscar);
	
	[OperationContract]
	DataSet BuscarRepuestos(string Buscar);
	
	[OperationContract]
	DataSet BuscarServicio(string Buscar);
	
	[OperationContract]
	DataSet BuscarVehiculo(string PLACA);
	
	[OperationContract]
	DataSet BusquedaEmpleado();
	
	[OperationContract]
	DataSet Busqueda_Estados();
	
	[OperationContract]
	DataSet BusquedaRoles();
	
	[OperationContract]
	DataSet ValidarLogin(string USUARIO, string CLAVE);
	
	[OperationContract]
	DataSet ELIMINAR_VEHICULOS(int ID_VEHICULO);
	
	[OperationContract]
	DataSet LLENADO_ORDENES();

	[OperationContract]
	DataSet ConsOrdenes(int ID_EMPLEADO);

	[OperationContract]
	DataSet DetalleOrden(int ID_ORDEN, int ID_SERVICIO, double CANTIDAD, int ID_REPUESTO);

	[OperationContract]
	DataSet DetOrdenes();

	[OperationContract]
	DataSet Genera_Factura(int ID_ORDEN, int ID_CLIENTE);

	[OperationContract]
	DataSet ImprimirFactura(int ID_ORDEN, DateTime FECHA);

	[OperationContract]
	DataSet Registro_de_Ordenes(int ID_CLIENTE, int ID_VEHICULO, DateTime FECHA, int ID_COTIZACION, int ID_EMPLEADO);

	[OperationContract]
	DataSet ReporteClientes();

	[OperationContract]
	DataSet ReporteEmpleados();

	[OperationContract]
	DataSet ReporteVehiculos();

	[OperationContract]
	DataSet ReporteFacturas(DateTime FECHA);
	

	[OperationContract]
	DataSet ListarVehiculos(int idcliente);

	[OperationContract]
	DataSet LlenadoDetalle();

	[OperationContract]
	DataSet Consultarorden(int idOrden);

	[OperationContract]
	DataSet BUSCAR_MODELO(int ID_MARCA);

	[OperationContract]
	DataSet BUSCAR_MARCA();

	[OperationContract]
	DataSet BuscarCotizacion(int ID_COTIZACION);

	[OperationContract]
	DataSet LlenadoFactura();

	[OperationContract]
	DataSet LlenadoFactura_Cliente(int ID_Orden);

	[OperationContract]
	DataSet Listar_Repuesto();

	[OperationContract]
	DataSet ConsOrdenes_Detalle(int ID_EMPLEADO,int ID_ORDEN);

	[OperationContract]
	DataSet ActualizarEstadoOrden(int ESTADO, int ID_ORDEN);

	[OperationContract]
	DataSet ListarCotizaciones();

	[OperationContract]
	DataSet ListarUsuarios();

	[OperationContract]
	DataSet ListarEmpleados();
	[OperationContract]
	DataSet ListarClientes();
	[OperationContract]
	DataSet ListarTablaREPUESTOS();

	[OperationContract]
	DataSet ListarTablaVehiculos();

	[OperationContract]
	DataSet EliminarDetalleOrden(int ID_DETALLE);

	[OperationContract]
	DataSet MODIFICAR_DetalleOrden(int ID_DETALLE, int ID_ORDEN, int ID_SERVICIO, int ID_REPUESTO, int Cantidad);


	// TODO: Add your service operations here
}



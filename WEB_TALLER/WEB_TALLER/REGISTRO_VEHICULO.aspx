﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="REGISTRO_VEHICULO.aspx.cs" Inherits="WEB_TALLER.REGISTRO_VEHICULO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style5 {
            width: 244px;
        }
        .auto-style6 {
            height: 23px;
            width: 244px;
        }
        .auto-style7 {
            width: 246px;
        }
        .auto-style8 {
            height: 23px;
            width: 246px;
        }
        .auto-style10 {
            height: 21px;
        }
        .auto-style11 {
            width: 244px;
            height: 21px;
        }
        .auto-style12 {
            width: 240px;
            height: 21px;
        }
        .auto-style25 {
            width: 87px;
        }
        .auto-style26 {
            height: 23px;
            width: 87px;
        }
        .auto-style27 {
            height: 21px;
            width: 87px;
        }
        .auto-style28 {
            width: 138px;
        }
        .auto-style29 {
            height: 23px;
            width: 138px;
        }
        .auto-style30 {
            height: 21px;
            width: 138px;
        }
        .auto-style34 {
            text-align: center;
        }
        .auto-style35 {
            font-size: xx-large;
        }
        .auto-style36 {
            width: 151px;
            font-size: large;
        }
        .auto-style37 {
            width: 138px;
            font-size: large;
        }
        .auto-style38 {
            width: 87px;
            font-size: large;
        }
        .auto-style39 {
            font-size: large;
        }
        .auto-style42 {
            height: 29px;
        }
        .auto-style45 {
            text-align: center;
            height: 29px;
        }
        .auto-style46 {
            width: 240px;
            font-size: large;
        }
        .auto-style47 {
            height: 23px;
            width: 240px;
        }
        .auto-style48 {
            width: 240px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="6"><span class="auto-style39">Administrativo:
                        </span>
                        <asp:Label ID="LblAdmon" runat="server" Text="Label" CssClass="auto-style39"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" class="auto-style34">
                        <br />
                        <span class="auto-style35"><strong>REGISTRO VEHICULOS</strong></span><br />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style42" colspan="3">
                        <asp:Label ID="LblIdVehiculo" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                    <td class="auto-style45" colspan="3">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style29">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                    <td class="auto-style47">&nbsp;</td>
                    <td class="auto-style8">&nbsp;</td>
                    <td class="auto-style26">
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style29">&nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                    <td class="auto-style47">&nbsp;</td>
                    <td class="auto-style8">&nbsp;</td>
                    <td class="auto-style26">
                        &nbsp;</td>
                    <td class="auto-style2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style37">Placa:</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="TxbPlaca" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style46">Año:</td>
                    <td class="auto-style7">
                        <asp:TextBox ID="TxbAnio" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style38">VIN:</td>
                    <td>
                        <asp:TextBox ID="TxbVIN" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style28">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style30"></td>
                    <td class="auto-style11"></td>
                    <td class="auto-style12">Color:</td>
                    <td class="auto-style27">
                        <asp:TextBox ID="TxbColor" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style10">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style28">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style37">Marca Vehiculo:</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="DdlMarca" runat="server" Height="28px" Width="208px" AutoPostBack="True" OnSelectedIndexChanged="Modelo">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style46">Modelo Vehiculo:</td>
                    <td class="auto-style7">
                        <asp:DropDownList ID="DdlModelo" runat="server" Height="28px" Width="208px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style36">Cliente:</td>
                    <td>
                        <asp:DropDownList ID="DdlCliente" runat="server" Height="28px" Width="208px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style28">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style28">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style28">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style34" colspan="3">
                        <asp:Button ID="BtnCrear" runat="server" Text="Crear" Height="35px" Width="150px" OnClick="BtnCrear_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                    <td class="auto-style34" colspan="3">
                        <asp:Button ID="BtnModificar" runat="server" Text="Modificar" Height="35px" Width="150px" OnClick="BtnModificar_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style28">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:GridView ID="GvVehiculos" runat="server" CellPadding="4" GridLines="Horizontal" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="1016px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

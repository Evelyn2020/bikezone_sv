﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class GENERAR_REPORTES : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblGerente.Text = Session["Usuario"].ToString();

                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_GERENTE.aspx");
        }

   
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class GESTIONAR_INVENTARIO : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            ds = ws.ListarTablaREPUESTOS();
            GvRepuestos.DataSource = ds;
            GvRepuestos.DataMember = ds.Tables[0].TableName;
            GvRepuestos.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblGerente.Text = Session["Usuario"].ToString();

                }
             
            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_GERENTE.aspx");
        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                
                ds = ws.AGREGAR_REPUESTOS(TxbNombreRepuesto.Text, double.Parse(TxbPrecioRepuestos.Text), int.Parse(TxbExistencias.Text));
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro agregado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.BuscarRepuestos(TxbBuscar.Text);

                GvRepuestos.DataSource = ds;
                GvRepuestos.DataMember = ds.Tables[0].TableName;
                GvRepuestos.DataBind();
                //if (ds != null)
                //{
                //    if (ds.Tables.Count > 0)
                //    {
                //        if (ds.Tables[0].Rows.Count > 0)
                //        {
                //            LblMensaje.Text = "Registro encontrados con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();                   
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.MODIFICAR_REPUESTOS(int.Parse(LblIdRepuesto.Text), TxbNombreRepuesto.Text, double.Parse(TxbPrecioRepuestos.Text), int.Parse(TxbExistencias.Text));
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro modificado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }

                        else
                            LblMensaje.Text = "Error en la recuperación del nuevo IdRepuesto Favor validar datos.";
                    }
                    else
                        LblMensaje.Text = "Error en la Modificacion del repuesto. Favor validar datos.";
                }
                else
                    LblMensaje.Text = "Error en la ejecución de la consulta!. Favor validar datos.";


            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void GvRepuestos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIdRepuesto.Text = GvRepuestos.SelectedRow.Cells[1].Text;
                TxbNombreRepuesto.Text = GvRepuestos.SelectedRow.Cells[2].Text;
                TxbPrecioRepuestos.Text = GvRepuestos.SelectedRow.Cells[3].Text;
                TxbExistencias.Text = GvRepuestos.SelectedRow.Cells[4].Text;
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class CONSULTAR_COTIZACION : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            ds = ws.ListarCotizaciones();
            GvCotizaciones.DataSource = ds;
            GvCotizaciones.DataMember = ds.Tables[0].TableName;
            GvCotizaciones.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblAdmon.Text = Session["Usuario"].ToString();
                    
                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }
        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_ADMON.aspx");
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                int Cotizacion = int.Parse(TxbBuscarCotizacion.Text);
                ds = ws.BuscarCotizacion(Cotizacion);
                GvCotizaciones.DataSource = ds;
                GvCotizaciones.DataMember = ds.Tables[0].TableName;
                GvCotizaciones.DataBind();
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
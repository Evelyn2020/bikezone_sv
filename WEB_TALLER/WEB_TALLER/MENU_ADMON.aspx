﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MENU_ADMON.aspx.cs" Inherits="WEB_TALLER.MENU_ADMON" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 653px;
        }
        .auto-style3 {
            text-align: center;
        }
        .auto-style4 {
            width: 653px;
            text-align: center;
        }
        .auto-style7 {
            width: 653px;
            text-align: center;
            height: 22px;
        }
        .auto-style8 {
            height: 22px;
            text-align: center;
        }
        .auto-style9 {
            font-size: large;
        }
        .auto-style10 {
            text-align: center;
            font-size: large;
        }
        .auto-style11 {
            width: 653px;
            text-align: center;
            font-size: large;
        }
        .auto-style12 {
            text-align: right;
        }
        .auto-style13 {
            font-size: xx-large;
        }
        .auto-style14 {
            text-align: center;
            font-size: large;
            height: 24px;
        }
        .auto-style15 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2"><span class="auto-style9"><span class="auto-style3"><asp:Image ID="Image1" runat="server" Height="45px" ImageUrl="~/Resources/LOGO.jpg" Width="100px" />
                        </span>
                        Administrativo:
                        </span>
                        <asp:Label ID="LblAdmon" runat="server" Text="Label" CssClass="auto-style9"></asp:Label>
                    </td>
                    <td class="auto-style12">
                        <asp:Label ID="lblFecha" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style3">
                        <br />
                        <strong><span class="auto-style13">MENU ADMINISTRATIVO</span></strong><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:ImageButton ID="IbtnRegistrarCliente" runat="server" Height="125px" ImageUrl="~/Resources/cliente.png" OnClick="IbtnRegistrarCliente_Click" Width="125px" />
                    </td>
                    <td class="auto-style8">
                        <asp:ImageButton ID="IbtnRegistrarVehiculo" runat="server" Height="125px" ImageUrl="~/Resources/coche.png" OnClick="IbtnRegistrarVehiculo_Click" Width="125px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11">Registro Cliente</td>
                    <td class="auto-style10">Registro Vehiculo</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:ImageButton ID="IbtnCotizacion" runat="server" Height="125px" ImageUrl="~/Resources/cotizacion admon.png" OnClick="IbtnCotizacion_Click" Width="125px" />
                    </td>
                    <td class="auto-style3">
                        <asp:ImageButton ID="IbtnOrden" runat="server" Height="125px" ImageUrl="~/Resources/orden admon.png" OnClick="IbtnOrden_Click" Width="125px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11">Consultar Cotizacion</td>
                    <td class="auto-style10">Crear Orden</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3" colspan="2">
                        <asp:ImageButton ID="IbtnFactura" runat="server" Height="125px" ImageUrl="~/Resources/factura.png" Width="125px" OnClick="IbtnFactura_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style14" colspan="2">Generar factura</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3" colspan="2">
                        <strong>
                        <asp:Button ID="BtnCerrarSesion" runat="server" OnClick="BtnCerrarSesion_Click" Text="Cerrar Sesion" BackColor="Red" Height="35px" Width="150px" BorderColor="Black" CssClass="auto-style15" />
                        </strong>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

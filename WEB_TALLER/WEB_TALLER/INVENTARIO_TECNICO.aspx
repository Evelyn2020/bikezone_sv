﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="INVENTARIO_TECNICO.aspx.cs" Inherits="WEB_TALLER.INVENTARIO_TECNICO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 692px;
        }
        .auto-style4 {
            font-size: large;
        }
        .auto-style6 {
            height: 21px;
        }
        .auto-style7 {
            height: 21px;
            font-size: large;
        }
        .auto-style8 {
            font-size: xx-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td colspan="2"><span class="auto-style4">Tecnico Automotriz:
                    </span>
                    <asp:Label ID="LblTecnico" runat="server" Text="Label" CssClass="auto-style4"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="auto-style2">
                    <br />
                    <span class="auto-style8"><strong>INVENTARIO DE REPUESTOS</strong></span><br />
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>
                    <asp:Button ID="BtnIrCotizacion" runat="server" Text="Ir a Cotizacion" OnClick="BtnIrCotizacion_Click" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="LblMensaje" runat="server" CssClass="auto-style4" ForeColor="Red"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="auto-style7">Nombre Repuesto:&nbsp;
                    <asp:TextBox ID="TxbBuscarRe" runat="server" Height="20px" Width="300px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="BtnBuscar" runat="server" Height="35px" OnClick="BtnBuscar_Click" Text="Buscar Repuesto" BackColor="#3399FF" BorderColor="Black" Width="175px" />
                    </td>
                <td class="auto-style6">
                    </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="2">
                        <FooterStyle BackColor="White" ForeColor="#333333" />
                        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#487575" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#275353" />
                    </asp:GridView>
                        <asp:GridView ID="GvInventarioTec" runat="server" CellPadding="4" GridLines="Horizontal" Width="1081px" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web.Design;
using System.Data;

namespace WEB_TALLER
{
    public partial class ImpresionFactura : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int IdOrden = int.Parse(Session["IdOrden"].ToString());
                int IdCliente = int.Parse(Session["IdCliente"].ToString());
                ds = ws.Genera_Factura(IdOrden, IdCliente);
                ReportDocument rd = new ReportDocument();
                rd.Load("FacturaImprimir.rpt");
                rd.SetDataSource(ds);
                CrystalReportViewer1.ReportSource = rd;
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnCerrar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/DETALLE_ORDEN.aspx");

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB_TALLER
{
    public partial class DETALLE_ORDEN : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        DataSet dsDetalle = new DataSet();
        DataSet dsfactura = new DataSet();
        int idOrden = 0; 
        int idCliente = 0; 
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Session["Usuario"] == null)
                        Response.Redirect("~/LOGIN.aspx");
                    else
                    {
                        LblAdmon.Text = Session["Usuario"].ToString();
                    }

                    //Orden
                    ds = ws.LlenadoDetalle();
                    DdlOrden.DataSource = ds.Tables[0];
                    DdlOrden.DataValueField = "ID_ORDEN";
                    DdlOrden.DataTextField = "Nombre";
                    DdlOrden.DataBind();
                    //servicio
                    ds = ws.LlenadoDetalle();
                    DdlServicios.DataSource = ds.Tables[1];
                    DdlServicios.DataValueField = "ID_SERVICIO";
                    DdlServicios.DataTextField = "NOMBRE_SERVICIO";
                    DdlServicios.DataBind();
                    //respuesto
                    ds = ws.LlenadoDetalle();
                    DdlRepuestos.DataSource = ds.Tables[2];
                    DdlRepuestos.DataValueField = "ID_REPUESTO";
                    DdlRepuestos.DataTextField = "NOMBRE_REPUESTO";
                    DdlRepuestos.DataBind();


                    //Orden GVdetalle
                    ds = ws.LlenadoDetalle();
                    DdlOrdenDetalle.DataSource = ds.Tables[0];
                    DdlOrdenDetalle.DataValueField = "ID_ORDEN";
                    DdlOrdenDetalle.DataTextField = "Nombre";
                    DdlOrdenDetalle.DataBind();


                }
                else
                {
                    if (Session["Usuario"] == null)
                        Response.Redirect("~/LOGIN.aspx");
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_ADMON.aspx");
        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                //Variables para el proceso de creación de la orden
                int idOrden = int.Parse(DdlOrden.SelectedValue);
                int idServicio = int.Parse(DdlServicios.SelectedValue);
                int cantidad = int.Parse(TxbCantidad.Text);
                int idRepuesto = int.Parse(DdlRepuestos.SelectedValue);

                //Se crea el encabezado de la orden de compra
                dsDetalle = ws.DetalleOrden(idOrden, idServicio, cantidad, idRepuesto);
                //Recuperar el IdOrden creada
                Session["IdDetalle"] = dsDetalle.Tables[0].Rows[0][0].ToString();
                int IdDet = int.Parse(Session["IdDetalle"].ToString());
            }

            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.MODIFICAR_DetalleOrden(int.Parse(LblIdDetalle.Text), int.Parse(DdlOrden.SelectedValue), int.Parse(DdlServicios.SelectedValue), int.Parse(DdlRepuestos.SelectedValue), int.Parse(TxbCantidad.Text));
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro modificado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }

                        else
                            LblMensaje.Text = "Error en la recuperación del nuevo IdDetalleOrden Favor validar datos.";
                    }
                    else
                        LblMensaje.Text = "Error en la Modificacion del detalle. Favor validar datos.";
                }
                else
                    LblMensaje.Text = "Error en la ejecución de la consulta!. Favor validar datos.";


            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }

        protected void btnMostrarDetalle_Click(object sender, EventArgs e)
        {
            try
            {
                //Variables para el proceso de creación de la orden
                int IdOrden = int.Parse(DdlOrdenDetalle.SelectedValue);
                //Se data set
                ds = ws.Consultarorden(IdOrden);

                GvDetalle.DataSource = ds;
                GvDetalle.DataMember = ds.Tables[0].TableName;
                GvDetalle.DataBind();
            }

            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }

      
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {

                if (GvDetalle.SelectedIndex >= 0)
                {
                    int IdDetalle = int.Parse(GvDetalle.SelectedRow.Cells[1].Text);
                    ds = ws.EliminarDetalleOrden(IdDetalle);
                    LblMensaje.Text = "Registro Eliminado con éxito";


                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }

        protected void BtnGenerarFactura_Click(object sender, EventArgs e)
        {
      
            Response.Redirect("~/Factura.aspx");

        }

        protected void GvDetalle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIdDetalle.Text = GvDetalle.SelectedRow.Cells[1].Text;
                DdlOrden.Text = GvDetalle.SelectedRow.Cells[2].Text;
                DdlServicios.SelectedItem.Text= GvDetalle.SelectedRow.Cells[4].Text;
                DdlRepuestos.SelectedItem.Text = GvDetalle.SelectedRow.Cells[6].Text;
                TxbCantidad.Text = GvDetalle.SelectedRow.Cells[8].Text;

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
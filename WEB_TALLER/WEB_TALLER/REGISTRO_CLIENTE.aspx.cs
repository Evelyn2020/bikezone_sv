﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class REGISTRO_CLIENTE : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {

            ds = ws.ListarClientes();
            GvClientes.DataSource = ds;
            GvClientes.DataMember = ds.Tables[0].TableName;
            GvClientes.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblAdmon.Text = Session["Usuario"].ToString();
                   
                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_ADMON.aspx");
        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.AGREGAR_CLIENTES(TxbNombreCli.Text, TxbApellidosCli.Text, TxbDUI.Text, TxbNIT.Text, TxbDireccion.Text, TxbTelefono.Text, TxbEmail.Text);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = ds.Tables[0].Rows[0][0].ToString();

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LblAdmon.Text = ex.Message;
            }
        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.MODIFICAR_CLIENTES(int.Parse(LblIdCliente.Text),TxbNombreCli.Text, TxbApellidosCli.Text, TxbDUI.Text, TxbNIT.Text, TxbDireccion.Text, TxbTelefono.Text, TxbEmail.Text);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro modificado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }

                        else
                            LblMensaje.Text = "Error en la recuperación del nuevo IdCliente Favor validar datos.";
                    }
                    else
                        LblMensaje.Text = "Error en la Modificacion del Cliente. Favor validar datos.";
                }
                else
                    LblMensaje.Text = "Error en la ejecución de la consulta!. Favor validar datos.";


            }
            catch (Exception ex)
            {
                LblAdmon.Text = ex.Message;
            }

        }

        protected void GvClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIdCliente.Text = GvClientes.SelectedRow.Cells[1].Text;
                TxbNombreCli.Text = GvClientes.SelectedRow.Cells[2].Text;
                TxbApellidosCli.Text = GvClientes.SelectedRow.Cells[3].Text;
                TxbDUI.Text = GvClientes.SelectedRow.Cells[4].Text;
                TxbNIT.Text = GvClientes.SelectedRow.Cells[5].Text;
                TxbDireccion.Text = GvClientes.SelectedRow.Cells[6].Text;
                TxbTelefono.Text = GvClientes.SelectedRow.Cells[7].Text;
                TxbEmail.Text = GvClientes.SelectedRow.Cells[8].Text;

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }


        }
    }
}
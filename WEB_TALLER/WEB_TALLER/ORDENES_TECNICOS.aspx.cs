﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class ORDENES_TECNICOS : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        DataSet dsDetalle = new DataSet();
        int ID_EMPLEADO = 0;

        protected void Page_Load(object sender, EventArgs e)
        {        
            LblUsuario.Text = Session["ID_USUARIO"].ToString();
            ID_EMPLEADO = int.Parse(LblUsuario.Text);
            ds = ws.ConsOrdenes(ID_EMPLEADO);

            GvOrdenesTec.DataSource = ds;
            GvOrdenesTec.DataMember = ds.Tables[0].TableName;
            GvOrdenesTec.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblTecnico.Text = Session["Usuario"].ToString();
                    //LblUsuario.Text = Session["ID_USUARIO"].ToString();
                }
            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }
        }
        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_TECNICO.aspx");
        }

        protected void GvOrdenesTec_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (GvOrdenesTec.SelectedIndex >= 0)
                {
                    ID_EMPLEADO = int.Parse(LblUsuario.Text);
                    int ID_ORDEN = 0;
                    dsDetalle = ws.ConsOrdenes_Detalle(ID_EMPLEADO,ID_ORDEN);
                    GvMostrarDetalle.DataSource = dsDetalle;
                    GvMostrarDetalle.DataMember = dsDetalle.Tables[0].TableName;
                    GvMostrarDetalle.DataBind();                
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnVerDetalle_Click(object sender, EventArgs e)
        {
            try
            {
                if (GvOrdenesTec.SelectedIndex >= 0)
                {
                    ID_EMPLEADO = int.Parse(LblUsuario.Text);
                    int ID_ORDEN = int.Parse(GvOrdenesTec.SelectedRow.Cells[1].Text);                 
                    dsDetalle = ws.ConsOrdenes_Detalle(ID_EMPLEADO, ID_ORDEN);
                    GvMostrarDetalle.DataSource = dsDetalle;
                    GvMostrarDetalle.DataMember = dsDetalle.Tables[0].TableName;
                    GvMostrarDetalle.DataBind();                   
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (GvOrdenesTec.SelectedIndex >= 0)
                {
                    int Estado = 1;
                    int ID_ORDEN = int.Parse(GvOrdenesTec.SelectedRow.Cells[1].Text);
                    ds = ws.ActualizarEstadoOrden(Estado, ID_ORDEN);                    
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }

        protected void BtnTerminado_Click(object sender, EventArgs e)
        {
            try
            {
                if (GvOrdenesTec.SelectedIndex >= 0)
                {
                    int Estado = 2;
                    int ID_ORDEN = int.Parse(GvOrdenesTec.SelectedRow.Cells[1].Text);
                    ds = ws.ActualizarEstadoOrden(Estado, ID_ORDEN);
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
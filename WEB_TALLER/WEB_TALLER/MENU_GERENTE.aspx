﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MENU_GERENTE.aspx.cs" Inherits="WEB_TALLER.MENU_GERENTE" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style4 {
            text-align: center;
            font-size: large;
        }
        .auto-style5 {
            text-align: center;
            height: 23px;
            font-size: large;
            width: 656px;
        }
        .auto-style6 {
            height: 23px;
            text-align: center;
        }
        .auto-style7 {
            text-align: right;
        }
        .auto-style8 {
            width: 656px;
        }
        .auto-style9 {
            text-align: center;
            width: 656px;
        }
        .auto-style10 {
            text-align: center;
            font-size: large;
            width: 656px;
        }
        .auto-style11 {
            font-size: xx-large;
        }
        .auto-style14 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style8"><span class="auto-style3"><asp:Image ID="Image1" runat="server" Height="45px" ImageUrl="~/Resources/LOGO.jpg" Width="100px" />
&nbsp;Gerente:
                        </span>
                        <asp:Label ID="LblGerente" runat="server" Text="Label" CssClass="auto-style3"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="auto-style7">
                        <asp:Label ID="lblFecha" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style2">
                        <br />
                        <strong><span class="auto-style11">MENU GERENTE</span></strong><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:ImageButton ID="IbtnEmpleado" runat="server" Height="150px" ImageUrl="~/Resources/empleado.png" OnClick="IbtnEmpleado_Click" Width="150px" />
                    </td>
                    <td class="auto-style2">
                        <asp:ImageButton ID="IbtnInventario" runat="server" Height="150px" ImageUrl="~/Resources/inventario.png" OnClick="IbtnInventario_Click" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">Registrar Empleado</td>
                    <td class="auto-style6">Gestionar Inventario</td>
                </tr>
                <tr>
                    <td class="auto-style8">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:ImageButton ID="IbtnReportes" runat="server" Height="150px" ImageUrl="~/Resources/reporte.png" OnClick="IbtnReportes_Click" Width="150px" />
                    </td>
                    <td class="auto-style2">
                        <asp:ImageButton ID="IbtnAgregarUsuario" runat="server" Height="150px" ImageUrl="~/Resources/agregarusuario.png" Width="150px" OnClick="IbtnAgregarUsuario_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style10">Generar Reportes</td>
                    <td class="auto-style4">Agregar Usuario</td>
                </tr>
                <tr>
                    <td class="auto-style8">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <strong>
                        <asp:Button ID="BtnCerrarSesion" runat="server" OnClick="BtnCerrarSesion_Click" Text="Cerrar Sesion" BackColor="Red" Height="35px" Width="150px" BorderColor="Black" CssClass="auto-style14" />
                        </strong>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB_TALLER
{
    public partial class COTIZAR_TECNICO : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        DataSet dsModelo = new DataSet();
        DataSet dsCotizar_tecnico = new DataSet();

        int ID_COTIZACION = 0;
        string NOMBRE = "";
        string APELLIDO = "";
        string MARCA = "";
        string MODELO = "";
        int ANIO = 0;
        DateTime Fecha = new DateTime(1998, 04, 30);
        string SERVICIO = "";
        int CANTIDAD = 0;
        string REPUESTO = "";
        double PRECIOU = 0;
        double PRECIOT = 0;
        double TOTAL = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            ds = ws.ListarCotizaciones();
            GvCotizaciones.DataSource = ds;
            GvCotizaciones.DataMember = ds.Tables[0].TableName;
            GvCotizaciones.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblTecnico.Text = Session["Usuario"].ToString();
                }
                Seleccion_Marca();

                //Llenado Repuestos
                ds = ws.LlenadoDetalle();
                DdlRepuestos.DataSource = ds.Tables[2];
                DdlRepuestos.DataValueField = "ID_REPUESTO";
                DdlRepuestos.DataTextField = "NOMBRE_REPUESTO";
                DdlRepuestos.DataBind();
                DdlRepuestos.Items.Insert(0, new ListItem("[Selecionar]", "0"));

                //Llenado Servicios
                ds = ws.LlenadoDetalle();
                DdlServicio.DataSource = ds.Tables[1];
                DdlServicio.DataValueField = "ID_SERVICIO";
                DdlServicio.DataTextField = "NOMBRE_SERVICIO";
                DdlServicio.DataBind();
                DdlServicio.Items.Insert(0, new ListItem("[Selecionar]", "0"));
            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }
        }

        protected void Seleccion_Marca()
        {
            try
            {
                ds = ws.BUSCAR_MARCA();
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DdlMarca.DataSource = ds.Tables[0];
                            DdlMarca.DataValueField = "ID_MARCA";
                            DdlMarca.DataTextField = "NOMBRE_MARCA";
                            DdlMarca.DataBind();
                            DdlMarca.Items.Insert(0, new ListItem("[Selecionar]", "0"));                          
                        }
                        else
                            LblMensaje.Text = "No se encontro ninguna vehiculo de esa Marca";
                    }
                    else
                        LblMensaje.Text = "No se encontraron datos";
                }
                else
                    LblMensaje.Text = "No se pudo ejecutar la consulta";
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_TECNICO.aspx");
        }

        protected void BtnConsultarInventario_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/INVENTARIO_TECNICO.aspx");
        }

        protected void BtnGuardar_Click1(object sender, EventArgs e)
        {
            try
            {
                NOMBRE = TxbNombreCli.Text;
                APELLIDO = TxbApellidoCli.Text;
                MARCA = DdlMarca.SelectedItem.Text;
                MODELO = DdlModelo.SelectedItem.Text;
                ANIO = int.Parse(TxbAnio.Text);
                Fecha = DateTime.Parse(TxbFecha.Text);
                SERVICIO = DdlServicio.SelectedItem.Text;
                CANTIDAD = int.Parse(TxbCantidad.Text);
                REPUESTO = DdlRepuestos.SelectedItem.Text;
                PRECIOU = double.Parse(TxbPrecio.Text);
                PRECIOT = double.Parse(TxbPrecioTotal.Text);
                TOTAL = double.Parse(TxbTotalPago.Text);

                dsCotizar_tecnico = ws.AGREGAR_COTIZACION(NOMBRE, APELLIDO, MARCA, MODELO, ANIO, Fecha, SERVICIO, CANTIDAD, REPUESTO, PRECIOU, PRECIOT, TOTAL);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Cotizacion agregada con éxito" + ds.Tables[0].Rows[0][0].ToString();
                            Session["ID_COTIZACION"] = dsCotizar_tecnico.Tables[0].Rows[0][0].ToString();
                            ID_COTIZACION = int.Parse(Session["ID_COTIZACION"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ID_COTIZACION = int.Parse(LblIdCotizacion.Text);
                NOMBRE = TxbNombreCli.Text;
                APELLIDO = TxbApellidoCli.Text;
                MARCA = DdlMarca.SelectedItem.Text;
                MODELO = DdlModelo.SelectedItem.Text;
                ANIO = int.Parse(TxbAnio.Text);
                Fecha = DateTime.Parse(TxbFecha.Text);
                SERVICIO = DdlServicio.SelectedItem.Text;
                CANTIDAD = int.Parse(TxbCantidad.Text);
                REPUESTO = DdlRepuestos.SelectedItem.Text;
                PRECIOU = double.Parse(TxbPrecio.Text);
                PRECIOT = double.Parse(TxbPrecioTotal.Text);
                TOTAL = double.Parse(TxbTotalPago.Text);

                dsCotizar_tecnico = ws.MODIFICA_COTIZACION(ID_COTIZACION, NOMBRE, APELLIDO, MARCA, MODELO, ANIO, Fecha, SERVICIO, CANTIDAD, REPUESTO, PRECIOU, PRECIOT, TOTAL);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            //Session["ID_COTIZACION"] = dsCotizar_tecnico.Tables[0].Rows[0][0].ToString();
                            //ID_COTIZACION = int.Parse(Session["ID_COTIZACION"].ToString());
                            LblMensaje.Text = "Cotizacion modificada con éxito" + ds.Tables[0].Rows[0][0].ToString();
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void Buscar_Modelo(object sender, EventArgs e)
        {
            try
            {
                int Id_marca = Convert.ToInt32(DdlMarca.SelectedValue);
                dsModelo = ws.BUSCAR_MODELO(Id_marca);
                if (dsModelo != null)
                {
                    if (dsModelo.Tables.Count > 0)
                    {
                        if (dsModelo.Tables[0].Rows.Count > 0)
                        {
                            DdlModelo.DataSource = dsModelo.Tables[0];
                            DdlModelo.DataTextField = "NOMBRE_MODELO";
                            DdlModelo.DataValueField = "ID_MODELOS";
                            DdlModelo.DataBind();
                            DdlModelo.Items.Insert(0, new ListItem("[Selecionar]", "0"));
                        }
                        else
                            LblMensaje.Text = "No se encontro ningun modelo";
                    }
                    else
                        LblMensaje.Text = "No se encontraron datos";
                }
                else
                    LblMensaje.Text = "No se pudo ejecutar la consulta";
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ID_COTIZACION = int.Parse(LblIdCotizacion.Text);

                ds = ws.BuscarCotizacion(ID_COTIZACION);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            TxbNombreCli.Text = ds.Tables[0].Rows[0]["NOMBRE"].ToString();
                            TxbApellidoCli.Text = ds.Tables[0].Rows[0]["APELLIDO"].ToString();
                            DdlMarca.SelectedItem.Text = ds.Tables[0].Rows[0]["MARCA"].ToString();
                            DdlModelo.SelectedValue = ds.Tables[0].Rows[0]["MODELO"].ToString();
                            TxbAnio.Text = ds.Tables[0].Rows[0]["ANIO"].ToString();
                            TxbFecha.Text = ds.Tables[0].Rows[0]["Fecha"].ToString();
                            DdlServicio.SelectedValue = ds.Tables[0].Rows[0]["SERVICIO"].ToString();
                            TxbCantidad.Text = ds.Tables[0].Rows[0]["CANTIDAD"].ToString();
                            DdlRepuestos.SelectedValue = ds.Tables[0].Rows[0]["REPUESTO"].ToString();
                            TxbPrecio.Text = ds.Tables[0].Rows[0]["PRECIOU"].ToString();
                            TxbPrecioTotal.Text = ds.Tables[0].Rows[0]["PRECIOT"].ToString();
                            TxbTotalPago.Text = ds.Tables[0].Rows[0]["TOTAL"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void GvCotizaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIdCotizacion.Text = GvCotizaciones.SelectedRow.Cells[1].Text;
                TxbNombreCli.Text = GvCotizaciones.SelectedRow.Cells[2].Text;
                TxbApellidoCli.Text = GvCotizaciones.SelectedRow.Cells[3].Text;
                DdlMarca.SelectedItem.Text = GvCotizaciones.SelectedRow.Cells[4].Text;
                DdlModelo.Text = GvCotizaciones.SelectedRow.Cells[5].Text;
                TxbAnio.Text = GvCotizaciones.SelectedRow.Cells[6].Text;
                TxbFecha.Text = GvCotizaciones.SelectedRow.Cells[7].Text;
                DdlServicio.SelectedItem.Text = GvCotizaciones.SelectedRow.Cells[8].Text;
                TxbCantidad.Text = GvCotizaciones.SelectedRow.Cells[9].Text;
                DdlRepuestos.SelectedItem.Text = GvCotizaciones.SelectedRow.Cells[10].Text;
                TxbPrecio.Text = GvCotizaciones.SelectedRow.Cells[11].Text;
                TxbTotalPago.Text = GvCotizaciones.SelectedRow.Cells[12].Text;
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }        
    }
}
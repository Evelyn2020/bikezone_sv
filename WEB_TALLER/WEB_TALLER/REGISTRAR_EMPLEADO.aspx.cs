﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class REGISTRAR_EMPLEADO : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {

            ds = ws.ListarEmpleados();
            GvEmpleados.DataSource = ds;
            GvEmpleados.DataMember = ds.Tables[0].TableName;
            GvEmpleados.DataBind();
            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblGerente.Text = Session["Usuario"].ToString();
                    
                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_GERENTE.aspx");
        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.AGREGAR_EMPLEADOS(TxbNombreEmpleado.Text, TxbApellidoEmpleado.Text, TxbDireccion.Text, TxbTelefono.Text, TxbEmail.Text, TxbDUI.Text, TxbNIT.Text, double.Parse(TxbSalario.Text));
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro agregado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.MODIFICAR_EMPLEADOS(int.Parse(LblIdEmpleado.Text),TxbNombreEmpleado.Text, TxbApellidoEmpleado.Text, TxbDireccion.Text, TxbTelefono.Text, TxbEmail.Text, TxbDUI.Text, TxbNIT.Text, double.Parse(TxbSalario.Text));
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro modificado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }

                        else
                            LblMensaje.Text = "Error en la recuperación del nuevo IdEmpleado Favor validar datos.";
                    }
                    else
                        LblMensaje.Text = "Error en la Modificacion del empleado. Favor validar datos.";
                }
                else
                    LblMensaje.Text = "Error en la ejecución de la consulta!. Favor validar datos.";
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void GvEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIdEmpleado.Text = GvEmpleados.SelectedRow.Cells[1].Text;
                TxbNombreEmpleado.Text = GvEmpleados.SelectedRow.Cells[2].Text;
                TxbApellidoEmpleado.Text = GvEmpleados.SelectedRow.Cells[3].Text;
                TxbDUI.Text = GvEmpleados.SelectedRow.Cells[4].Text;
                TxbNIT.Text = GvEmpleados.SelectedRow.Cells[5].Text;
                TxbDireccion.Text = GvEmpleados.SelectedRow.Cells[6].Text;
                TxbTelefono.Text = GvEmpleados.SelectedRow.Cells[7].Text;
                TxbEmail.Text = GvEmpleados.SelectedRow.Cells[8].Text;
                TxbSalario.Text = GvEmpleados.SelectedRow.Cells[9].Text;

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
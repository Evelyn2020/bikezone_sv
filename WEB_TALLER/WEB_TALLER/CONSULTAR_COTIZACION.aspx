﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CONSULTAR_COTIZACION.aspx.cs" Inherits="WEB_TALLER.CONSULTAR_COTIZACION" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 598px;
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style5 {
            text-align: center;
        }
        .auto-style6 {
            width: 598px;
            text-align: right;
        }
        .auto-style4 {
            font-size: large;
        }
        .auto-style7 {
            font-size: xx-large;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="2"><span class="auto-style3">Administrativo:
                        </span>
                        <asp:Label ID="LblAdmon" runat="server" Text="Label" CssClass="auto-style3"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style5">
                        <br />
                        <span class="auto-style7"><strong>CONSULTA DE COTIZACIONES</strong></span><strong><br />
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2"><span class="auto-style3"><asp:Label ID="LblMensaje" runat="server" CssClass="auto-style4" ForeColor="Red"></asp:Label>
                        </span>
                    </td>
                    <td class="auto-style5">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2"><span class="auto-style3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style6"><span class="auto-style3">Cotizacion:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TxbBuscarCotizacion" runat="server" Height="20px" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                    <asp:Button ID="BtnBuscar" runat="server" Height="30px" OnClick="BtnBuscar_Click" Text="Buscar Cotizacion" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GvCotizaciones" runat="server" CellPadding="4" GridLines="Horizontal" Width="981px" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

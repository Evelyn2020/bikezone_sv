﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Factura.aspx.cs" Inherits="WEB_TALLER.Factura" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">



        .auto-style1 {
            width: 100%;
        }
        .auto-style7 {
            font-size: large;
        }
        .auto-style4 {
            text-align: center;
        }
        .auto-style2 {
            width: 137px;
        }
        .auto-style5 {
            width: 261px;
        }
        .auto-style3 {
            width: 142px;
        }
        .auto-style6 {
            width: 267px;
        }
        .auto-style9 {
            width: 137px;
            font-size: large;
        }
        .auto-style8 {
            width: 142px;
            font-size: large;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <table class="auto-style1">
            <tr>
                <td colspan="6"><span class="auto-style7">Administrativo: </span>
                    <asp:Label ID="LblAdmon" runat="server" CssClass="auto-style7" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="6">FACTURA</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4" colspan="3">
                    <asp:Button ID="BtnMenuPrincipal" runat="server" Height="35px" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Width="175px" />
                </td>
            </tr>
            <tr>
                <td id="LblMensaje0" class="auto-style2">
                    &nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">&nbsp;Orden:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="DdlOrden" runat="server" AutoPostBack="True" Height="28px" Width="208px" OnSelectedIndexChanged="Cliente">
                    </asp:DropDownList>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td class="auto-style7">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">Cliente:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="DdlCliente" runat="server" Height="28px" Width="208px" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">
                    <asp:Button ID="BtnImprimirFactura" runat="server" Text="Imprimir Factura" OnClick="BtnImprimirFactura_Click" />
                </td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">&nbsp;</td>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style6">
                    &nbsp;</td>
                <td class="auto-style7">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </table>
        </div>
    </form>
</body>
</html>

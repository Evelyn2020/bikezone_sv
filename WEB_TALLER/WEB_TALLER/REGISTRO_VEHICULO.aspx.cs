﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class REGISTRO_VEHICULO : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        DataSet dsMarca= new DataSet();
        DataSet dsModelo = new DataSet();
        DataSet dsCliente = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            ds = ws.ListarTablaVehiculos();
            GvVehiculos.DataSource = ds;
            GvVehiculos.DataMember = ds.Tables[0].TableName;
            GvVehiculos.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblAdmon.Text = Session["Usuario"].ToString();                    
                }
                
                
                dsCliente = ws.BuscarCliente("");

            
                DdlCliente.DataSource = dsCliente;
                DdlCliente.DataMember = dsCliente.Tables[0].TableName;
                DdlCliente.DataTextField = "NOMBRE";
                DdlCliente.DataValueField = "ID_CLIENTE";
                DdlCliente.DataBind();

                LlenarMarca();

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }


        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_ADMON.aspx");
        }

        private void LlenarMarca()
        {

            try
            {
                dsMarca = ws.BUSCAR_MARCA();
                if (dsMarca != null)
                {
                    if (dsMarca.Tables.Count > 0)
                    {
                        if (dsMarca.Tables[0].Rows.Count > 0)
                        {
                            DdlMarca.DataSource = dsMarca;
                            DdlMarca.DataMember = dsMarca.Tables[0].TableName;
                            DdlMarca.DataTextField = "NOMBRE_MARCA";
                            DdlMarca.DataValueField = "ID_MARCA";
                            DdlMarca.DataBind();
                        }
                        else
                            LblMensaje.Text = "No se encontro ninguna Marca";
                    }
                    else
                        LblMensaje.Text = "No se encontraron datos";
                }
                else
                    LblMensaje.Text = "No se pudo ejecutar la consulta";


            }
            catch (Exception ex)
            {
                LblAdmon.Text = ex.Message;
            }
        }

        protected void Modelo(object sender, EventArgs e)
        {
            try
            {
                int ID_MARCA = int.Parse(DdlMarca.SelectedValue);

                dsModelo = ws.BUSCAR_MODELO(ID_MARCA);
                if (dsModelo != null)
                {
                    if (dsModelo.Tables.Count > 0)
                    {
                        if (dsModelo.Tables[0].Rows.Count > 0)
                        {

                            DdlModelo.DataSource = dsModelo;
                            DdlModelo.DataMember = dsModelo.Tables[0].TableName;
                            DdlModelo.DataTextField = "NOMBRE_MODELO";
                            DdlModelo.DataValueField = "ID_MODELOS";
                            DdlModelo.DataBind();
                        }
                        else
                            LblMensaje.Text = "No se encontro ninguna Marca";
                    }
                    else
                        LblMensaje.Text = "No se encontraron datos";
                }
                else
                    LblMensaje.Text = "No se pudo ejecutar la consulta";


            }
            catch (Exception ex)
            {
                LblAdmon.Text = ex.Message;
            }


        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try 
            { 
                ds = ws.AGREGAR_VEHICULOS(TxbPlaca.Text, int.Parse(TxbAnio.Text),TxbVIN.Text, TxbColor.Text, int.Parse(DdlMarca.Text), int.Parse(DdlModelo.Text), int.Parse(DdlCliente.Text));
                if(ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            TxbPlaca.Text = ds.Tables[0].Rows[0][0].ToString();
                            LblMensaje.Text = "Registro agregado con éxito";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LblAdmon.Text = ex.Message;
            }
        }


        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ds = ws.MODIFICAR_VEHICULOS(int.Parse(LblIdVehiculo.Text),TxbPlaca.Text, int.Parse(TxbAnio.Text), TxbVIN.Text, TxbColor.Text, int.Parse(DdlMarca.Text), int.Parse(DdlModelo.Text), int.Parse(DdlCliente.Text));
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro modificado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }

                        else
                            LblMensaje.Text = "Error en la recuperación del nuevo IdVehiculo Favor validar datos.";
                    }
                    else
                        LblMensaje.Text = "Error en la Modificacion del Vehiculo. Favor validar datos.";
                }
                else
                    LblMensaje.Text = "Error en la ejecución de la consulta!. Favor validar datos.";


            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIdVehiculo.Text = GvVehiculos.SelectedRow.Cells[1].Text;
                TxbPlaca.Text = GvVehiculos.SelectedRow.Cells[2].Text;
                TxbAnio.Text = GvVehiculos.SelectedRow.Cells[3].Text;
                TxbVIN.Text = GvVehiculos.SelectedRow.Cells[4].Text;
                TxbColor.Text = GvVehiculos.SelectedRow.Cells[5].Text;
                DdlMarca.Text = GvVehiculos.SelectedRow.Cells[6].Text;
                DdlModelo.Text = GvVehiculos.SelectedRow.Cells[7].Text;
                DdlCliente.Text = GvVehiculos.SelectedRow.Cells[8].Text;

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ORDENES_TECNICOS.aspx.cs" Inherits="WEB_TALLER.ORDENES_TECNICOS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style7 {
            height: 37px;
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style2 {
            text-align: right;
        }
        .auto-style4 {
            height: 23px;
            font-size: large;
        }
        .auto-style5 {
            height: 21px;
        }
        .auto-style6 {
            height: 166px;
            text-align: center;
        }
        .auto-style8 {
            height: 23px;
        }
        .auto-style9 {
            text-align: center;
        }
        .auto-style10 {
            font-size: xx-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style7">
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                </tr>
                <tr>
                    <td><span class="auto-style3">Tecnico Automotriz:
                        </span>
                        <asp:Label ID="LblTecnico" runat="server" Text="Label" CssClass="auto-style3"></asp:Label>
                        <asp:Label ID="LblUsuario" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <br />
                        <strong><span class="auto-style10">ORDENES ASIGNADAS A TECNICO</span></strong><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" Text="Ir a Menu Principal" OnClick="BtnMenuPrincipal_Click" BackColor="#3399FF" BorderColor="Black" Font-Size="Small" Height="35px" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                    <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Button ID="BtnVerDetalle" runat="server" OnClick="BtnVerDetalle_Click" Text="Ver Detalle " BackColor="#3399FF" BorderColor="Black" Height="35px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvOrdenesTec" runat="server" CellPadding="4" GridLines="Horizontal" OnSelectedIndexChanged="GvOrdenesTec_SelectedIndexChanged" Width="982px" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" />
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="White" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#487575" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#275353" />
                                </asp:GridView>
                                <div class="auto-style9">
                                    <br />
                                    <asp:Button ID="BtnProcesando" runat="server" BackColor="#3399FF" BorderColor="Black" Height="35px" OnClick="Button1_Click" Text="Pasar a Procesando" />
                                    <asp:Button ID="BtnTerminado" runat="server" BackColor="#3399FF" BorderColor="Black" Height="35px" OnClick="BtnTerminado_Click" Text="Pasar a Terminada" />
                                    <br />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"></td>
                </tr>
                <tr>
                    <td class="auto-style6">
                        <asp:GridView ID="GvMostrarDetalle" runat="server" CellPadding="4" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="984px">
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                </table>
        </div>
    </form>
</body>
</html>

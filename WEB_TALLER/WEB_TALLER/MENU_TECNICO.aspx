﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MENU_TECNICO.aspx.cs" Inherits="WEB_TALLER.MENU_TECNICO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 664px;
        }
        .auto-style3 {
            text-align: center;
        }
        .auto-style4 {
            width: 664px;
            text-align: center;
        }
        .auto-style5 {
            font-size: large;
        }
        .auto-style6 {
            width: 664px;
            text-align: center;
            font-size: x-large;
        }
        .auto-style7 {
            text-align: center;
            font-size: x-large;
        }
        .auto-style9 {
            font-size: large;
        }
        .auto-style10 {
            text-align: right;
        }
        .auto-style11 {
            font-weight: bold;
        }
        .auto-style12 {
            font-size: xx-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2"><span class="auto-style5"><span class="auto-style9"><span class="auto-style3"><asp:Image ID="Image1" runat="server" Height="45px" ImageUrl="~/Resources/LOGO.jpg" Width="100px" />
                        </span>
                        </span>
                        Tecnico Automotriz:
                        </span>
                        <asp:Label ID="LblTecnico" runat="server" Text="Label" CssClass="auto-style5"></asp:Label>
                    </td>
                    <td class="auto-style10">
                        <asp:Label ID="lblFecha" runat="server" Text="Label" CssClass="auto-style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style3">
                        <br />
                        <span class="auto-style12"><strong>MENU TECNICO</strong></span><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:ImageButton ID="IbtnCotizar" runat="server" Height="150px" ImageUrl="~/Resources/cotizacion.png" OnClick="IbtnCotizar_Click" Width="150px" />
                    </td>
                    <td class="auto-style3">
                        <asp:ImageButton ID="IbtnOrdenTec" runat="server" Height="150px" ImageUrl="~/Resources/orden.png" OnClick="IbtnOrdenTec_Click" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">
                        Cotizar</td>
                    <td class="auto-style7">
                        Ordenes Asignadas</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3" colspan="2">
                        <strong>
                        <asp:Button ID="BtnCerrarSesion" runat="server" Text="Cerrar Sesion" OnClick="BtnCerrarSesion_Click1" BackColor="Red" Font-Size="Medium" Height="35px" Width="150px" CssClass="auto-style11" />
                        </strong>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

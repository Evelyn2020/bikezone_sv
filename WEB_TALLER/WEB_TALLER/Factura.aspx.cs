﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB_TALLER
{
    public partial class Factura : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();     
        DataSet dsfactura = new DataSet();
        DataSet dsOrden = new DataSet();
        DataSet dsCliente = new DataSet();
        int idOrden = 0;
        int idCliente = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
           try
           {
                if (!Page.IsPostBack)
                {
                    if (Session["Usuario"] == null)
                        Response.Redirect("~/LOGIN.aspx");
                    else
                    {
                        LblAdmon.Text = Session["Usuario"].ToString();
                    }

                    Orden();

                }
                else
                {
                    if (Session["Usuario"] == null)
                        Response.Redirect("~/LOGIN.aspx");
                }
           }
            catch (Exception ex)
           {
                LblMensaje.Text = ex.Message;
           }
        }

        protected void Orden()
        {
            //Orden
            dsOrden = ws.LlenadoFactura();
            DdlOrden.DataSource = dsOrden.Tables[0];
            DdlOrden.DataValueField = "ID_ORDEN";
            DdlOrden.DataTextField = "ID_ORDEN";
            DdlOrden.DataBind();
        }

        protected void Cliente(object sender, EventArgs e)
        {
            int IdOrden = int.Parse(DdlOrden.SelectedValue);
           // servicio
            dsCliente = ws.LlenadoFactura_Cliente(IdOrden);
            DdlCliente.DataSource = dsCliente.Tables[0];
            DdlCliente.DataValueField = "ID_CLIENTE";
            DdlCliente.DataTextField = "Nombre";
            DdlCliente.DataBind();
        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_ADMON.aspx");
        }

        protected void BtnImprimirFactura_Click(object sender, EventArgs e)
        {
            Session["IdOrden"] = int.Parse(DdlOrden.SelectedValue);
            Session["IdCliente"] = int.Parse(DdlCliente.SelectedValue);
            Response.Redirect("~/ImpresionFactura.aspx");
        }

        protected void GVFactura_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
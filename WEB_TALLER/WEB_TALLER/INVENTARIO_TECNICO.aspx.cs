﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class INVENTARIO_TECNICO : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            ds = ws.Listar_Repuesto();
            GvInventarioTec.DataSource = ds;
            GvInventarioTec.DataMember = ds.Tables[0].TableName;
            GvInventarioTec.DataBind();

            if (!Page.IsPostBack)
            {           
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblTecnico.Text = Session["Usuario"].ToString();
                }            
            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }
        }

        protected void BtnIrCotizacion_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/COTIZAR_TECNICO.aspx");
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
               string NombreRep = TxbBuscarRe.Text;
               ds = ws.BuscarRepuestos(NombreRep);

               GvInventarioTec.DataSource = ds;
               GvInventarioTec.DataMember = ds.Tables[0].TableName;
               GvInventarioTec.DataBind();      
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
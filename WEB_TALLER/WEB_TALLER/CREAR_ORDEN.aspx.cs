﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class CREAR_ORDEN : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        DataSet dsPlaca = new DataSet();
        DataSet dsOrden = new DataSet();
        DateTime Fecha = new DateTime(1998, 04, 30);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Session["Usuario"] == null)
                        Response.Redirect("~/LOGIN.aspx");
                    else
                    {
                        LblAdmon.Text = Session["Usuario"].ToString();
                    }
                    //invocar metodo de llenado de droplist clientes
                    ClienteSelecionado();
                    //tecnicos
                    ds = ws.LLENADO_ORDENES();
                    DdlAsignarOrden.DataSource = ds.Tables[1];
                    DdlAsignarOrden.DataValueField = "ID_EMPLEADO";
                    DdlAsignarOrden.DataTextField = "NOMBRE EMPLEADO";
                    DdlAsignarOrden.DataBind();

                }
                else
                {
                    if (Session["Usuario"] == null)
                        Response.Redirect("~/LOGIN.aspx");
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        //metodo de llenado de droplist clientes
        protected void ClienteSelecionado()
        {
            try
            {
                ds = ws.LLENADO_ORDENES();
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DdlCliente.DataSource = ds.Tables[0];
                            DdlCliente.DataValueField = "ID_CLIENTE";
                            DdlCliente.DataTextField = "Nombre";
                            DdlCliente.DataBind();
                            DdlCliente.Items.Insert(0, new ListItem("[Selecionar]", "0"));
                            DdlPlaca.Items.Insert(0, new ListItem("[Selecionar]", "0"));
                        }
                        else
                            LblMensaje.Text = "No se encontro ninguna Cliente";
                    }
                    else
                        LblMensaje.Text = "No se encontraron datos";
                }
                else
                    LblMensaje.Text = "No se pudo ejecutar la consulta";

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }

        }


        //metodo de llenado de droplist placa
        protected void Placa(object sender, EventArgs e)
        {
            try
            {
                int IdCliente = Convert.ToInt32(DdlCliente.SelectedValue);
                dsPlaca = ws.ListarVehiculos(IdCliente);
                if (dsPlaca != null)
                {
                    if (dsPlaca.Tables.Count > 0)
                    {
                        if (dsPlaca.Tables[0].Rows.Count > 0)
                        {
                            DdlPlaca.DataSource = dsPlaca.Tables[0];
                            DdlPlaca.DataTextField = "PLACA";
                            DdlPlaca.DataValueField = "ID_VEHICULO";
                            DdlPlaca.DataBind();
                            DdlPlaca.Items.Insert(0, new ListItem("[Selecionar]", "0"));
                        }
                        else
                            LblMensaje.Text = "No se encontro ninguna Placa";
                    }
                    else
                        LblMensaje.Text = "No se encontraron datos";
                }
                else
                    LblMensaje.Text = "No se pudo ejecutar la consulta";

            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
        //boton ir a menu principal
        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_ADMON.aspx");
        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                //Variables para el proceso de creación de la orden
                int idCliente = int.Parse(DdlCliente.SelectedValue);
                int idVehiculo = int.Parse(DdlPlaca.SelectedValue);
                Fecha = DateTime.Parse(TxbFecha.Text);
                int idCotizacion = int.Parse(TxbCotizacion.Text);
                int idEmpleado = int.Parse(DdlAsignarOrden.SelectedValue);
                //Se crea el encabezado de la orden de compra
                dsOrden = ws.Registro_de_Ordenes(idCliente, idVehiculo, Fecha, idCotizacion, idEmpleado);
                //Recuperar el IdOrden creada
                Session["IdOrden"] = dsOrden.Tables[0].Rows[0][0].ToString();
                int IdOrden = int.Parse(Session["IdOrden"].ToString());
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
            Response.Redirect("~/DETALLE_ORDEN.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           
            Response.Redirect("~/DETALLE_ORDEN.aspx");
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="REGISTRO_CLIENTE.aspx.cs" Inherits="WEB_TALLER.REGISTRO_CLIENTE" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style7 {
            text-align: center;
        }
        .auto-style27 {
            font-size: large;
        }
        .auto-style36 {
            width: 33%;
            height: 23px;
        }
        .auto-style48 {
            width: 33%;
            height: 21px;
        }
        .auto-style50 {
            width: 13%;
            height: 21px;
        }
        .auto-style51 {
            width: 13%;
            height: 23px;
        }
        .auto-style55 {
            width: 13%;
            height: 26px;
        }
        .auto-style58 {
            width: 33%;
            height: 26px;
        }
        .auto-style60 {
            width: 36%;
            height: 21px;
        }
        .auto-style61 {
            width: 36%;
            height: 26px;
        }
        .auto-style62 {
            width: 36%;
            height: 23px;
        }
        .auto-style64 {
            height: 21px;
            width: 121px;
        }
        .auto-style66 {
            height: 26px;
            width: 121px;
        }
        .auto-style67 {
            height: 23px;
            width: 121px;
        }
        .auto-style68 {
            width: 13%;
        }
        .auto-style69 {
            width: 36%;
        }
        .auto-style72 {
            width: 13%;
            font-size: large;
        }
        .auto-style74 {
            width: 121px;
        }
        .auto-style75 {
            width: 33%;
        }
        .auto-style76 {
            font-size: large;
            width: 121px;
        }
        .auto-style8 {
            font-size: xx-large;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td>
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="auto-style7">
                        <br />
                        <span class="auto-style8"><strong>REGISTRO CLIENTE<br />
                        </strong></span><br />
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><span class="auto-style27">Administrativo:
                        </span>
                        <asp:Label ID="LblAdmon" runat="server" Text="Label" CssClass="auto-style27"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style68">
                        <asp:Label ID="LblIdCliente" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style68">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style50"></td>
                    <td class="auto-style60"></td>
                    <td class="auto-style64"></td>
                    <td class="auto-style48"></td>
                </tr>
                <tr>
                    <td class="auto-style72">Nombre Cliente:</td>
                    <td class="auto-style69">
                        <asp:TextBox ID="TxbNombreCli" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style76">Apellidos Cliente</td>
                    <td class="auto-style75">
                        <asp:TextBox ID="TxbApellidosCli" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style55"></td>
                    <td class="auto-style61"></td>
                    <td class="auto-style66"></td>
                    <td class="auto-style58"></td>
                </tr>
                <tr>
                    <td class="auto-style51"></td>
                    <td class="auto-style62"></td>
                    <td class="auto-style67"></td>
                    <td class="auto-style36"></td>
                </tr>
                <tr>
                    <td class="auto-style72">DUI:</td>
                    <td class="auto-style69">
                        <asp:TextBox ID="TxbDUI" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style76">NIT:</td>
                    <td class="auto-style75">
                        <asp:TextBox ID="TxbNIT" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style68">&nbsp;</td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style68">&nbsp;</td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style72">Direccion:</td>
                    <td class="auto-style69">
                        <asp:TextBox ID="TxbDireccion" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style76">Telefono:</td>
                    <td class="auto-style75">
                        <asp:TextBox ID="TxbTelefono" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style68">&nbsp;</td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style68">&nbsp;</td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style72">E-mail:</td>
                    <td class="auto-style69">
                        <asp:TextBox ID="TxbEmail" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style68">&nbsp;</td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style7" colspan="2">
                        <asp:Button ID="BtnCrear" runat="server" Text="Crear" Height="35px" Width="150px" OnClick="BtnCrear_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                    <td class="auto-style7" colspan="2">
                        <asp:Button ID="BtnModificar" runat="server" Text="Modificar" Height="35px" Width="150px" OnClick="BtnModificar_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style68">&nbsp;</td>
                    <td class="auto-style69">&nbsp;</td>
                    <td class="auto-style74">&nbsp;</td>
                    <td class="auto-style75">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="GvClientes" runat="server" CellPadding="4" GridLines="Horizontal" OnSelectedIndexChanged="GvClientes_SelectedIndexChanged" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="1188px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
                </table>
        </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_TALLER
{
    public partial class MENU_TECNICO : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblTecnico.Text = Session["Usuario"].ToString();
                    lblFecha.Text = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }
        }
        protected void IbtnCotizar_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/COTIZAR_TECNICO.aspx");
        }
        protected void IbtnOrdenTec_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/ORDENES_TECNICOS.aspx");
        }
       
        protected void BtnCerrarSesion_Click1(object sender, EventArgs e)
        {
            Session["Usuario"] = null;
            Response.Redirect("~/LOGIN.aspx");
        }
    }
}
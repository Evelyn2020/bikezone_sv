﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class AGREGAR_USUARIO : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet(); 
        DataSet dsEmpleado = new DataSet();
        DataSet dsRoles = new DataSet();
        DataSet dsModificar = new DataSet();

        string USUARIO = "";
        string CLAVE = "";
        int ID_EMPLEADO = 0;
        int ID_ROL = 0;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            ds = ws.ListarUsuarios();
            GvUsuarios.DataSource = ds;
            GvUsuarios.DataMember = ds.Tables[0].TableName;
            GvUsuarios.DataBind();

            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblGerente.Text = Session["Usuario"].ToString();

                }

                dsRoles = ws.BusquedaRoles();
                DdlRol.DataSource = dsRoles.Tables[0];
                DdlRol.DataValueField = "ID_ROL";
                DdlRol.DataTextField = "NOMBRE_ROL";
                DdlRol.DataBind();

                dsEmpleado = ws.BusquedaEmpleado();
                DdlEmpleado.DataSource = dsEmpleado.Tables[0];
                DdlEmpleado.DataValueField = "ID_EMPLEADO";
                DdlEmpleado.DataTextField = "NOMBRE EMPLEADO";
                DdlEmpleado.DataBind();
            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnMenuPrincipal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MENU_GERENTE.aspx");
        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                USUARIO = TxtUsuario.Text;
                CLAVE = TxtClave.Text;
                ID_EMPLEADO = int.Parse(DdlEmpleado.SelectedValue);
                ID_ROL = int.Parse(DdlRol.SelectedValue);

                ds = ws.AGREGAR_USUARIOS(USUARIO, CLAVE, ID_EMPLEADO, ID_ROL);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            LblMensaje.Text = "Registro agregado con éxito, Id = " + ds.Tables[0].Rows[0][0].ToString();
                        }
                        else {
                            LblMensaje.Text = "Error al agregar Registro";
                        }
                    }
                    else
                    {
                        LblMensaje.Text = "Error rows menor q cero";
                    }
                }
                else
                {
                    LblMensaje.Text = "Error ds vacio";
                }
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {                                
                dsModificar = ws.MODIFICAR_USUARIOS(int.Parse(LblIDUsuario.Text), TxtUsuario.Text, TxtClave.Text, int.Parse(DdlEmpleado.SelectedValue), int.Parse(DdlRol.SelectedValue));
                    if (dsModificar != null)
                    {
                        if (dsModificar.Tables.Count > 0)
                        {
                            if (dsModificar.Tables[0].Rows.Count > 0)
                            {
                            LblMensaje.Text = "Registro modificado con éxito";  //+ ds.Tables[0].Rows[0][0].ToString();
                            }
                            else
                                LblMensaje.Text = "Error en la recuperación del nuevo IdUsuario Favor validar datos.";
                        }
                        else
                            LblMensaje.Text = "Error en la Modificacion del usuario. Favor validar datos.";
                    }
                    else
                        LblMensaje.Text = "Error en la ejecución de la consulta!. Favor validar datos.";
                
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }

        protected void GvUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LblIDUsuario.Text = GvUsuarios.SelectedRow.Cells[1].Text;
                TxtUsuario.Text= GvUsuarios.SelectedRow.Cells[2].Text;
                TxtClave.Text= GvUsuarios.SelectedRow.Cells[3].Text;
                DdlEmpleado.Text = GvUsuarios.SelectedRow.Cells[4].Text;
                DdlRol.Text = GvUsuarios.SelectedRow.Cells[5].Text;         
              
            }
            catch (Exception ex)
            {
                LblMensaje.Text = ex.Message;
            }
        }
    }
}
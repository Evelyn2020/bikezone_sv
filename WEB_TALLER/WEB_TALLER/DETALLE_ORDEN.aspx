﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DETALLE_ORDEN.aspx.cs" Inherits="WEB_TALLER.DETALLE_ORDEN" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">


        .auto-style1 {
            width: 100%;
        }
        .auto-style7 {
            font-size: large;
        }
        .auto-style4 {
            text-align: center;
        }
        .auto-style2 {
            width: 137px;
        }
        .auto-style5 {
            width: 261px;
        }
        .auto-style3 {
            width: 142px;
        }
        .auto-style6 {
            width: 267px;
        }
        .auto-style9 {
            width: 137px;
            font-size: large;
        }
        .auto-style8 {
            width: 142px;
            font-size: large;
        }
        .auto-style14 {
            height: 49px;
        }
        .auto-style15 {
            font-size: xx-large;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
        <table class="auto-style1">
            <tr>
                <td colspan="6"><span class="auto-style7">Administrativo: </span>
                    <asp:Label ID="LblAdmon" runat="server" CssClass="auto-style7" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="6">
                    <br />
                    <strong><span class="auto-style15">DETALLE DE ORDEN</span><br class="auto-style15" />
                    </strong>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="LblIdDetalle" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4" colspan="3">
                    <asp:Button ID="BtnMenuPrincipal" runat="server" Height="35px" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                </td>
            </tr>
            <tr>
                <td id="LblMensaje0" class="auto-style2">
                    <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">&nbsp;Orden:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="DdlOrden" runat="server" AutoPostBack="True" Height="28px" Width="208px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style8">Repuestos:</td>
                <td class="auto-style6">
                    <asp:DropDownList ID="DdlRepuestos" runat="server" Height="28px" Width="208px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style7">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">Servicios:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="DdlServicios" runat="server" Height="28px" Width="208px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style8">Cantidad:</td>
                <td class="auto-style6">
                    <asp:TextBox ID="TxbCantidad" runat="server" Height="20px" Width="100px" TextMode="Number"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="6">
                    <asp:Button ID="BtnCrear" runat="server" Height="35px" OnClick="BtnCrear_Click" Text="Agregar" Width="150px" BackColor="Lime" BorderColor="Black" />
                </td>
            </tr>
            <tr>
                <td class="auto-style9">&nbsp;</td>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style6">
                    &nbsp;</td>
                <td class="auto-style7">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">Buscar Orden:</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="DdlOrdenDetalle" runat="server" AutoPostBack="True" Height="28px" Width="208px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style3">
                    <asp:Button ID="btnMostrarDetalle" runat="server" OnClick="btnMostrarDetalle_Click" Text="Mostrar Detalle" BackColor="#3399FF" BorderColor="Black" Height="35px" Width="175px" />
                </td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4" colspan="3">
                    <asp:Button ID="btnEliminar" runat="server" OnClick="btnEliminar_Click" Text="Eliminar" Height="35px" Width="175px" BackColor="Lime" BorderColor="Black" />
                </td>
                <td class="auto-style4" colspan="3">
                    <asp:Button ID="BtnModificar" runat="server" Height="35px" OnClick="BtnModificar_Click" Text="Modificar" Width="175px" BackColor="Lime" BorderColor="Black" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style14" colspan="6">
                    <asp:GridView ID="GvDetalle" runat="server" CellPadding="4" GridLines="Horizontal" OnSelectedIndexChanged="GvDetalle_SelectedIndexChanged" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="801px">
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#333333" />
                        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#487575" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#275353" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style5">
                    <asp:Button ID="BtnGenerarFactura" runat="server" OnClick="BtnGenerarFactura_Click" Text="Generar Factura" BackColor="#3399FF" BorderColor="Black" Height="35px" Width="175px" />
                </td>
                <td class="auto-style3">
                    &nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>

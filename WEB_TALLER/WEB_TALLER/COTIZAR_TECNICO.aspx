﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="COTIZAR_TECNICO.aspx.cs" Inherits="WEB_TALLER.COTIZAR_TECNICO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 174px;
        }
        .auto-style3 {
            width: 255px;
        }
        .auto-style6 {
            width: 174px;
            height: 23px;
        }
        .auto-style7 {
            width: 255px;
            height: 23px;
        }
        .auto-style9 {
            height: 23px;
        }
        .auto-style11 {
            text-align: center;
        }
        .auto-style12 {
            font-size: large;
        }
        .auto-style13 {
            width: 174px;
            font-size: large;
        }
        .auto-style16 {
            height: 24px;
        }
        .auto-style20 {
            height: 24px;
            text-align: center;
        }
        .auto-style22 {
            width: 198px;
            height: 23px;
            font-size: large;
        }
        .auto-style23 {
            width: 198px;
        }
        .auto-style24 {
            width: 198px;
            font-size: large;
        }
        .auto-style25 {
            width: 198px;
            height: 23px;
        }
        .auto-style26 {
            width: 174px;
            font-size: large;
            height: 26px;
        }
        .auto-style27 {
            width: 255px;
            height: 26px;
        }
        .auto-style28 {
            width: 198px;
            font-size: large;
            height: 26px;
        }
        .auto-style29 {
            height: 26px;
        }
        .auto-style30 {
            width: 130px;
        }
        .auto-style32 {
            height: 24px;
            width: 130px;
        }
        .auto-style33 {
            height: 23px;
            width: 130px;
        }
        .auto-style34 {
            height: 26px;
            width: 130px;
            font-size: large;
        }
        .auto-style35 {
            text-align: center;
            width: 130px;
        }
        .auto-style36 {
            width: 130px;
            font-size: large;
        }
        .auto-style39 {
            height: 26px;
            width: 152px;
        }
        .auto-style40 {
            height: 23px;
            width: 152px;
        }
        .auto-style41 {
            width: 152px;
        }
        .auto-style42 {
            font-size: xx-large;
        }
        .auto-style43 {
            width: 174px;
            height: 23px;
            font-size: large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="6">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style16"><span class="auto-style12">Tecnico Automotriz: </span>
                                    <asp:Label ID="LblTecnico" runat="server" CssClass="auto-style12" Text="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style20">
                                    <br />
                                    <span class="auto-style42"><strong>COTIZACION</strong></span><br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style20" colspan="3">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" Text="Ir a Menu Principal" OnClick="BtnMenuPrincipal_Click" BackColor="#3399FF" BorderColor="Black" Font-Size="Small" Height="35px" Width="175px" />
                    </td>
                    <td class="auto-style20" colspan="3">
                        <asp:Button ID="BtnConsultarInventario" runat="server" Text="Consultar Inventario" OnClick="BtnConsultarInventario_Click" BackColor="#3399FF" BorderColor="Black" Font-Size="Small" Height="35px" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style16" colspan="4">
                        <asp:Label ID="LblIdCotizacion" runat="server" Visible="False" CssClass="auto-style12"></asp:Label>
                    </td>
                    <td class="auto-style32">
                        &nbsp;</td>
                    <td class="auto-style16">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style16" colspan="4">
                        <asp:Label ID="LblMensaje" runat="server" CssClass="auto-style12" ForeColor="#FF3300"></asp:Label>
                    </td>
                    <td class="auto-style32">
                        &nbsp;</td>
                    <td class="auto-style16">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">Nombre Cliente:</td>
                    <td class="auto-style3">
                        <asp:TextBox ID="TxbNombreCli" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style24">Apellido Cliente:</td>
                    <td class="auto-style41">
                        <asp:TextBox ID="TxbApellidoCli" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style30">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style26">Marca de Vehiculo:</td>
                    <td class="auto-style27">
                        <asp:DropDownList ID="DdlMarca" runat="server" Height="28px" Width="208px" AutoPostBack="True" OnSelectedIndexChanged="Buscar_Modelo">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style28">Modelo de Vehiculo:</td>
                    <td class="auto-style39">
                        <asp:DropDownList ID="DdlModelo" runat="server" Height="28px" Width="208px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style34">
                        Año Vehiculo:</td>
                    <td class="auto-style29">
                        <asp:TextBox ID="TxbAnio" runat="server" Height="20px" TextMode="Number" Width="125px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">Servicio:</td>
                    <td class="auto-style3">
                        <asp:DropDownList ID="DdlServicio" runat="server" Height="28px" Width="208px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style24">Fecha de Cotizacion:</td>
                    <td class="auto-style41">
                        <asp:TextBox ID="TxbFecha" runat="server" Height="20px" TextMode="Date" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style30">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style6"></td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style25">&nbsp;</td>
                    <td class="auto-style40">&nbsp;</td>
                    <td class="auto-style33">&nbsp;</td>
                    <td class="auto-style9">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">&nbsp;</td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>

                    <td class="auto-style30">&nbsp;</td>

                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style13">Repuestos:</td>
                    <td class="auto-style3">
                        <asp:DropDownList ID="DdlRepuestos" runat="server" Height="28px" Width="208px" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style24">Cantidad:</td>
                    <td class="auto-style41">
                        <asp:TextBox ID="TxbCantidad" runat="server" Height="20px" TextMode="Number" Width="50px"></asp:TextBox>
                    </td>

                    <td class="auto-style36">&nbsp;</td>

                    <td>
                        &nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style13">&nbsp;</td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>

                    <td class="auto-style30">&nbsp;</td>

                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td class="auto-style43">Precio:</td>
                    <td class="auto-style7">
                        <asp:TextBox ID="TxbPrecio" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style22">Precio Total:</td>
                    <td class="auto-style40">
                        <asp:TextBox ID="TxbPrecioTotal" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style33">
                        &nbsp;</td>
                    <td class="auto-style9">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">&nbsp;</td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">&nbsp;</td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style24">Total Pago:</td>
                    <td class="auto-style41">
                        <asp:TextBox ID="TxbTotalPago" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style30">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">&nbsp;</td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">&nbsp;</td>
                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style23">&nbsp;</td>
                    <td class="auto-style41">&nbsp;</td>
                    <td class="auto-style30">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style11" colspan="3">
                        <asp:Button ID="BtnGuardar" runat="server" BackColor="Lime" BorderColor="Black" Height="35px" OnClick="BtnGuardar_Click1" Text="Guardar" Width="150px" />
                    </td>
                    <td class="auto-style11" colspan="3">
                        <asp:Button ID="BtnModificar" runat="server" Text="Modificar" OnClick="BtnModificar_Click" BackColor="Lime" BorderColor="Black" Font-Size="Small" Height="35px" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11" colspan="2">
                        &nbsp;</td>
                    <td class="auto-style11" colspan="2">
                        &nbsp;</td>
                    <td class="auto-style35">
                        &nbsp;</td>
                    <td class="auto-style11">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style11" colspan="6">
                        <asp:GridView ID="GvCotizaciones" runat="server" CellPadding="4" GridLines="Horizontal" OnSelectedIndexChanged="GvCotizaciones_SelectedIndexChanged" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="1160px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
                </table>
        </div>
    </form>
</body>
</html>

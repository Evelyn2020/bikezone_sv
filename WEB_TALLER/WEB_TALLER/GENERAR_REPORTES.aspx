﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GENERAR_REPORTES.aspx.cs" Inherits="WEB_TALLER.GENERAR_REPORTES" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
            height: 30px;
        }
        .auto-style3 {
            width: 685px;
        }
        .auto-style4 {
            width: 685px;
            height: 30px;
        }
        .auto-style5 {
            font-size: large;
        }
        .auto-style6 {
            width: 685px;
            height: 23px;
        }
        .auto-style7 {
            height: 23px;
        }
        .auto-style8 {
            width: 685px;
            height: 40px;
        }
        .auto-style9 {
            height: 40px;
        }
        .auto-style10 {
            text-align: center;
        }
        .auto-style11 {
            font-size: xx-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><span class="auto-style5">Gerente:
                        </span>
                        <asp:Label ID="LblGerente" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style10">
                        <br />
                        <span class="auto-style11"><strong>REPORTES BIKE ZONE</strong></span><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4"></td>
                    <td class="auto-style2">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">
                        <asp:Menu ID="Menu2" runat="server">
                            <Items>
                                <asp:MenuItem Text="REPORTES" Value="REPORTES">
                                    <asp:MenuItem Text="Reportes Clientes" Value="mnuClientes" NavigateUrl="~/ReportCliente.aspx"></asp:MenuItem>
                                    <asp:MenuItem Text="Reportes Empleados" Value="mnuEmpleados" NavigateUrl="~/ReportCrystalEmpleados.aspx"></asp:MenuItem>
                                    <asp:MenuItem Text="Reportes Vehiculos" Value="mnuVehiculos" NavigateUrl="~/ReportCrystalVehiculos.aspx"></asp:MenuItem>
                                </asp:MenuItem>
                            </Items>
                        </asp:Menu>
                    </td>
                    <td class="auto-style9"></td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style6"></td>
                    <td class="auto-style7"></td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GESTIONAR_INVENTARIO.aspx.cs" Inherits="WEB_TALLER.GESTIONAR_INVENTARIO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style5 {
            font-size: large;
        }
        .auto-style6 {
            height: 23px;
        }
        .auto-style8 {
            width: 241px;
            height: 23px;
            font-size: large;
        }
        .auto-style12 {
            width: 278px;
        }
        .auto-style14 {
            height: 23px;
            width: 278px;
        }
        .auto-style20 {
            width: 184px;
            height: 23px;
            font-size: large;
        }
        .auto-style21 {
            width: 184px;
        }
        .auto-style22 {
            height: 23px;
            width: 184px;
        }
        .auto-style23 {
            width: 278px;
            text-align: center;
        }
        .auto-style26 {
            width: 278px;
            height: 13px;
        }
        .auto-style27 {
            width: 184px;
            height: 13px;
        }
        .auto-style28 {
            height: 13px;
        }
        .auto-style29 {
            font-size: xx-large;
        }
        .auto-style30 {
            width: 241px;
        }
        .auto-style31 {
            width: 241px;
            height: 13px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4"><span class="auto-style5">Gerente:
                        </span>
                        <asp:Label ID="LblGerente" runat="server" Text="Label" CssClass="auto-style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="auto-style2">
                        <br />
                        <strong><span class="auto-style29">REGISTRO REPUESTO</span></strong><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style30">
                        <asp:Label ID="LblIdRepuesto" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                    <td class="auto-style2" colspan="3">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style31">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style26"></td>
                    <td class="auto-style27"></td>
                    <td class="auto-style28"></td>
                </tr>
                <tr>
                    <td class="auto-style8">Nombre de Repuestos:</td>
                    <td class="auto-style14">
                        <asp:TextBox ID="TxbNombreRepuesto" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style20">Precio de Repuesto:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TxbPrecioRepuestos" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style30">&nbsp;</td>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style30">&nbsp;</td>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">Existencias de Repuestos:</td>
                    <td class="auto-style14">
                        <asp:TextBox ID="TxbExistencias" runat="server" Height="20px" TextMode="Number" Width="50px"></asp:TextBox>
                    </td>
                    <td class="auto-style22">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style30">&nbsp;</td>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <asp:Button ID="BtnCrear" runat="server" Height="35px" Text="Crear" Width="150px" OnClick="BtnCrear_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                    <td class="auto-style2" colspan="2">
                        <asp:Button ID="BtnModificar" runat="server" Height="35px" Text="Modificar" Width="150px" OnClick="BtnModificar_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style30">&nbsp;</td>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style30">&nbsp;</td>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style30"><span class="auto-style5">Filtrar:&nbsp;&nbsp;&nbsp;&nbsp; </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="TxbBuscar" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style23">
                        <asp:Button ID="BtnBuscar" runat="server" Height="35px" Text="Buscar Repuestos" Width="150px" OnClick="BtnBuscar_Click" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style30">&nbsp;</td>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style21">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="GvRepuestos" runat="server" CellPadding="4" GridLines="Horizontal" Width="955px" OnSelectedIndexChanged="GvRepuestos_SelectedIndexChanged" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
                </table>
        </div>
    </form>
</body>
</html>

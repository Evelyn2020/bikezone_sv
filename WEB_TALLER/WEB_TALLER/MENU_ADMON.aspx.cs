﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_TALLER
{
    public partial class MENU_ADMON : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblAdmon.Text = Session["Usuario"].ToString();
                    lblFecha.Text = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["Usuario"] = null;
            Response.Redirect("~/LOGIN.aspx");
        }

        protected void IbtnRegistrarCliente_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/REGISTRO_CLIENTE.aspx");
        }

        protected void IbtnRegistrarVehiculo_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/REGISTRO_VEHICULO.aspx");
        }

        protected void IbtnCotizacion_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/CONSULTAR_COTIZACION.aspx");
        }

        protected void IbtnOrden_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/CREAR_ORDEN.aspx");
        }

        protected void IbtnFactura_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/Factura.aspx");
        }
    }
}
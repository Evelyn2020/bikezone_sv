﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CREAR_ORDEN.aspx.cs" Inherits="WEB_TALLER.CREAR_ORDEN" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">

        .auto-style1 {
            width: 100%;
        }
        .auto-style7 {
            font-size: large;
        }
        .auto-style4 {
            text-align: center;
        }
        .auto-style2 {
            width: 137px;
        }
        .auto-style5 {
            width: 261px;
        }
        .auto-style3 {
            width: 142px;
        }
        .auto-style6 {
            width: 267px;
        }
        .auto-style9 {
            width: 137px;
            font-size: large;
        }
        .auto-style8 {
            width: 142px;
            font-size: large;
        }
        .auto-style10 {
            font-size: xx-large;
        }
        .auto-style11 {
            text-align: center;
            height: 39px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="6"><span class="auto-style7">Administrativo: </span>
                        <asp:Label ID="LblAdmon" runat="server" CssClass="auto-style7" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4" colspan="6">
                        <br />
                        <strong><span class="auto-style10">CREAR ORDEN</span></strong><br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11" colspan="3">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Ir a Detalle" BackColor="#3399FF" BorderColor="Black" Height="35px" Width="175px" />
                    </td>
                    <td class="auto-style11" colspan="3">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" Height="35px" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td id="LblMensaje0" class="auto-style2">
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">Cliente:</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="DdlCliente" runat="server" AutoPostBack="True" Height="28px" OnSelectedIndexChanged="Placa" Width="208px">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style8">Placa:</td>
                    <td class="auto-style6">
                        <asp:DropDownList ID="DdlPlaca" runat="server" AutoPostBack="True" Height="28px" Width="208px">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">Fecha Ingreso:</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="TxbFecha" runat="server" Height="20px" TextMode="Date" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style8">Cotizacion:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TxbCotizacion" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">Asignar Orden:</td>
                    <td class="auto-style5">
                        <asp:DropDownList ID="DdlAsignarOrden" runat="server" Height="28px" Width="208px">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style8">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4" colspan="6">
                        <asp:Button ID="BtnCrear" runat="server" Height="35px" OnClick="BtnCrear_Click" Text="Crear" Width="150px" BackColor="Lime" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

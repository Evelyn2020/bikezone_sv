﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LOGIN.aspx.cs" Inherits="WEB_TALLER.LOGIN" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/> <!--LogoPestaña-->
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            font-size: xx-large;
        }
        .auto-style3 {
            text-align: center;
            background-color: #FFFFFF;
        }
        .auto-style4 {
            width: 655px;
        }
        .auto-style6 {
            width: 655px;
            text-align: right;
            height: 25px;
        }
        .auto-style7 {
            height: 25px;
            font-size: large;
        }
        .auto-style9 {
            height: 37px;
            background-color: #FFFFFF;
            text-align: center;
        }
        .auto-style10 {
            text-align: center;
            height: 23px;
            background-color: #FFFFFF;
        }
        .auto-style12 {
            background-color: #FFFFFF;
        }
        .auto-style13 {
            width: 655px;
            background-color: #FFFFFF;
            height: 23px;
        }
        .auto-style15 {
            width: 655px;
            text-align: right;
            background-color: #FFFFFF;
            font-size: large;
        }
        body{
            background-image: url("~/Resources/fondo.jpg");
        }
        .auto-style16 {
            background-color: #FFFFFF;
            height: 23px;
        }
        .auto-style17 {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <table class="auto-style1">
                <tr>
                    <td class="auto-style3" colspan="2">
                        <br />
                        <br />
                        <span class="auto-style2"><strong>INICIA SESION</strong><br />
                        </span><br />
                        <asp:Image ID="Image1" runat="server" Height="150px" ImageUrl="~/Resources/LOGO.jpg" Width="400px" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style15">
                        <i class='fas fa-user-circle' style='font-size:24px;color:blue'></i>
                        Usuario:</td>
                    <td class="auto-style12"> 
                        <asp:TextBox ID="txbUsuario" runat="server" Width="200px" Height="20px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style15">
                        <i class='fas fa-lock' style='font-size:24px;color:blue'></i>
                        Contraseña:</td>
                    <td class="auto-style12">
                        <asp:TextBox ID="TxbContra" runat="server" Width="200px" TextMode="Password" Height="20px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style13"></td>
                    <td class="auto-style16"></td>
                </tr>
                <tr>
                    <td class="auto-style9" colspan="2">
                        <strong>
                        <asp:Button ID="BtnIniciarSesion" runat="server" OnClick="BtnIniciarSesion_Click" Text="Iniciar Sesion" BackColor="#3399FF" BorderColor="#0000CC" Font-Size="Medium" ForeColor="Black" Height="35px" Width="150px" CssClass="auto-style17" />
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style10" colspan="2">
                        <asp:Label ID="LblMensaje" runat="server" BackColor="White" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">
                        <asp:CheckBox ID="CkbCredenciales" runat="server" Height="25px" Width="25px" />
                    </td>
                    <td class="auto-style7">¿Recordar Credenciales?</td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="REGISTRAR_EMPLEADO.aspx.cs" Inherits="WEB_TALLER.REGISTRAR_EMPLEADO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            height: 23px;
            text-align: center;
        }
        .auto-style4 {
            font-size: xx-large;
        }
        .auto-style5 {
            font-size: large;
        }
        .auto-style6 {
            width: 271px;
        }
        .auto-style7 {
            width: 267px;
        }
        .auto-style8 {
            height: 23px;
        }
        .auto-style9 {
            width: 271px;
            height: 23px;
        }
        .auto-style10 {
            width: 267px;
            height: 23px;
        }
        .auto-style11 {
            width: 286px;
        }
        .auto-style12 {
            height: 23px;
            width: 286px;
        }
        .auto-style13 {
            height: 22px;
        }
        .auto-style14 {
            width: 271px;
            height: 22px;
        }
        .auto-style15 {
            width: 267px;
            height: 22px;
        }
        .auto-style16 {
            width: 286px;
            height: 22px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="6"><span class="auto-style5">Gerente:
                        </span>
                        <asp:Label ID="LblGerente" runat="server" Text="Label" CssClass="auto-style5"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="LblIdEmpleado" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                    <td class="auto-style2" colspan="3">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" OnClick="BtnMenuPrincipal_Click" Text="Ir a Menu Principal" Height="35px" Width="175px" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3" colspan="6">
                        <br />
                        <span class="auto-style4"><strong>REGISTRO EMPLEADOS</strong><br />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="auto-style6">
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">Nombre Empleado:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TxbNombreEmpleado" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style5">Apellido Empleado:</td>
                    <td class="auto-style7">
                        <asp:TextBox ID="TxbApellidoEmpleado" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13"></td>
                    <td class="auto-style14"></td>
                    <td class="auto-style13"></td>
                    <td class="auto-style15"></td>
                    <td class="auto-style13"></td>
                    <td class="auto-style16"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">DUI:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TxbDUI" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style5">NIT:</td>
                    <td class="auto-style7">
                        <asp:TextBox ID="TxbNIT" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">Direccion:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TxbDireccion" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style5">Telefono:</td>
                    <td class="auto-style7">
                        <asp:TextBox ID="TxbTelefono" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style5">E-mail:</td>
                    <td class="auto-style11">
                        <asp:TextBox ID="TxbEmail" runat="server" Height="20px" Width="200px" ForeColor="Black"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"></td>
                    <td class="auto-style9"></td>
                    <td class="auto-style8"></td>
                    <td class="auto-style10"></td>
                    <td class="auto-style8"></td>
                    <td class="auto-style12"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">Salario:</td>
                    <td class="auto-style6">
                        <asp:TextBox ID="TxbSalario" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style6">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="auto-style11">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8"></td>
                    <td class="auto-style9"></td>
                    <td class="auto-style8"></td>
                    <td class="auto-style10"></td>
                    <td class="auto-style8"></td>
                    <td class="auto-style12"></td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="3">
                        <asp:Button ID="BtnCrear" runat="server" Text="Crear" Height="35px" Width="150px" OnClick="BtnCrear_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                    <td class="auto-style2" colspan="3">
                        <asp:Button ID="BtnModificar" runat="server" Text="Modificar" Height="35px" Width="150px" OnClick="BtnModificar_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="3">
                        &nbsp;</td>
                    <td class="auto-style2" colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="6">
                        <asp:GridView ID="GvEmpleados" runat="server" OnSelectedIndexChanged="GvEmpleados_SelectedIndexChanged" CellPadding="4" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="1240px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

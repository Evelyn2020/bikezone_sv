﻿using System;
using System.Collections.Generic;
//using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

namespace WEB_TALLER
{
    public partial class LOGIN : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                HttpCookie cookieUsuario = Request.Cookies["Usuario"];
                HttpCookie cookiePassword = Request.Cookies["Password"];
                HttpCookie cookieRecordar = Request.Cookies["Recordar"];
                if (cookieRecordar != null)//Si existe la cookie Recordar
                {
                    if (cookieRecordar.Value == "1")
                    {
                        txbUsuario.Text = cookieUsuario.Value;
                        TxbContra.Text = cookiePassword.Value;
                        CkbCredenciales.Checked = true;
                    }
                    else
                    {
                        CkbCredenciales.Checked = false;
                        Session["Usuario"] = null;
                        Session["Password"] = null;
                    }
                }
            }
        }

        //ENTRAR MENUS
        protected void BtnIniciarSesion_Click(object sender, EventArgs e)
        {
            HttpCookie user = Request.Cookies["Usuario"];
            HttpCookie pass = Request.Cookies["Password"];
            if (CkbCredenciales.Checked)
            {
                if (TxbContra.Text.Trim().Length > 0)//Si el usuario ingresa un password
                {
                    HttpCookie cookiePassword = new HttpCookie("Password", TxbContra.Text);
                    cookiePassword.Expires = DateTime.Now.AddYears(5);
                    Response.Cookies.Add(cookiePassword);
                    Session["Password"] = TxbContra.Text;
                    Session["Usuario"] = txbUsuario.Text;
                }
                else
                {
                    if (pass.Value != null)
                    {
                        Session["Password"] = pass.Value;
                        Session["Usuario"] = user.Value;
                    }
                    else
                    {
                        Session["Password"] = TxbContra.Text;
                        Session["Usuario"] = txbUsuario.Text;
                    }
                }
            }

            //Creación y almacenamiento de las cookies
            if (CkbCredenciales.Checked)//Recordar usuario y contraseña
            {
                if (Session["Password"] == null)
                {
                    //Creando las cookies para recordar el usuario y contraseña
                    HttpCookie cookieUsuario = new HttpCookie("Usuario", txbUsuario.Text);
                    cookieUsuario.Expires = DateTime.Now.AddYears(5);
                    Response.Cookies.Add(cookieUsuario);//Se almacena la cookie con su valor en la PC del cliente

                    HttpCookie cookiePassword = new HttpCookie("Password", TxbContra.Text);
                    cookiePassword.Expires = DateTime.Now.AddYears(5);
                    Response.Cookies.Add(cookiePassword);

                    HttpCookie cookieRecordar = new HttpCookie("Recordar", "1");
                    cookieRecordar.Expires = DateTime.Now.AddYears(5);
                    Response.Cookies.Add(cookieRecordar);
                }
            }
            else//Vencer automáticamente las cookies de Usuario y Contraseña
            {
                HttpCookie cookieUsuario = new HttpCookie("Usuario", "");
                cookieUsuario.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookieUsuario);//Se almacena la cookie con su valor en la PC del cliente

                HttpCookie cookiePassword = new HttpCookie("Password", "");
                cookiePassword.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookiePassword);

                HttpCookie cookieRecordar = new HttpCookie("Recordar", "0");
                cookieRecordar.Expires = DateTime.Now.AddYears(5);
                Response.Cookies.Add(cookieRecordar);
            }
            if (Session["Password"] == null)
            {
                Session["Usuario"] = txbUsuario.Text;
                Session["Password"] = TxbContra.Text;
            }
            if (Session["Usuario"] != null && Session["Password"] != null)
            {
                //Ejecutar el método del WS para validar el usuario
                ds = ws.ValidarLogin(Session["Usuario"].ToString(), Session["Password"].ToString());
                if (ds != null)//Se valida si el DS se ejecutó sin problemas
                {
                    if (ds.Tables.Count > 0)//Valida que el DS tenga al menos un DataTable
                    {
                        if (ds.Tables[0].Rows.Count > 0)//Se valida que el DataTable tenga al menos un registro
                        {
                            if (ds.Tables[0].Rows[0]["ID_USUARIO"].ToString() != "-1")//Se valida que el usuario exista
                            {
                                Session["ID_USUARIO"] = int.Parse(ds.Tables[0].Rows[0]["ID_USUARIO"].ToString());
                                if (ds.Tables[0].Rows[0]["ID_ROL"].ToString() == "1")
                                {
                                    Response.Redirect("~/MENU_GERENTE.aspx");
                                }
                                else if (ds.Tables[0].Rows[0]["ID_ROL"].ToString() == "2")
                                {
                                    Response.Redirect("~/MENU_ADMON.aspx");
                                }
                                else if (ds.Tables[0].Rows[0]["ID_ROL"].ToString() == "3")
                                {
                                    Response.Redirect("~/MENU_TECNICO.aspx");

                                }
                                else
                                {
                                    LblMensaje.Text = "El usuario ingresado no existe!.";
                                }
                            }
                            else
                            {
                                LblMensaje.Text = "El usuario ingresado no existe!.";
                                Session["Usuario"] = null;
                            }
                        }
                        else
                        {
                            LblMensaje.Text = "No se encontraron registros!. Favor intente de nuevo.";
                            Session["Usuario"] = null;
                        }
                    }
                    else
                    {
                        LblMensaje.Text = "Sin registros!. Favor intente de nuevo.";
                        Session["Usuario"] = null;
                    }
                }
                else
                {
                    LblMensaje.Text = "No se pudo ejecutar la consulta!. Favor intente de nuevo.";
                    Session["Usuario"] = null;
                }
            }
            else
                LblMensaje.Text = "No hay una sesión activa!";
        }
    }
}
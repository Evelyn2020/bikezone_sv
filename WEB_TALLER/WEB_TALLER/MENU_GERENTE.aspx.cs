﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WEB_TALLER
{
    public partial class MENU_GERENTE : System.Web.UI.Page
    {
        Servicio_BikeZone.ServiceClient ws = new Servicio_BikeZone.ServiceClient();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
                else
                {
                    LblGerente.Text = Session["Usuario"].ToString();
                    lblFecha.Text = "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy");
                }

            }
            else
            {
                if (Session["Usuario"] == null)
                    Response.Redirect("~/LOGIN.aspx");
            }

        }

        protected void BtnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["Usuario"] = null;
            Response.Redirect("~/LOGIN.aspx");
        }

        protected void IbtnEmpleado_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/REGISTRAR_EMPLEADO.aspx");
        }

        protected void IbtnInventario_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/GESTIONAR_INVENTARIO.aspx");
        }

        protected void IbtnReportes_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/GENERAR_REPORTES.aspx");
        }

        protected void IbtnAgregarUsuario_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/AGREGAR_USUARIO.aspx");
        }
    }
}
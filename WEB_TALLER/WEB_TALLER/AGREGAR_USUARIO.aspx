﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AGREGAR_USUARIO.aspx.cs" Inherits="WEB_TALLER.AGREGAR_USUARIO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="shortcut icon" type="image/jpg" href="Resources/LOGO.jpg"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            font-size: large;
        }
        .auto-style3 {
            text-align: center;
        }
        .auto-style8 {
            width: 394px;
            font-size: large;
        }
        .auto-style9 {
            width: 757px;
            height: 23px;
            font-size: large;
        }
        .auto-style12 {
            width: 394px;
        }
        .auto-style13 {
            width: 394px;
            height: 23px;
        }
        .auto-style14 {
            width: 729px;
        }
        .auto-style15 {
            width: 729px;
            height: 23px;
        }
        .auto-style16 {
            width: 757px;
        }
        .auto-style17 {
            width: 757px;
            height: 23px;
        }
        .auto-style18 {
            text-align: center;
            width: 448px;
        }
        .auto-style19 {
            width: 448px;
        }
        .auto-style20 {
            width: 448px;
            height: 23px;
        }
        .auto-style21 {
            width: 394px;
            font-size: large;
            height: 25px;
        }
        .auto-style22 {
            width: 729px;
            height: 25px;
        }
        .auto-style23 {
            width: 757px;
            height: 25px;
            font-size: large;
        }
        .auto-style24 {
            width: 448px;
            height: 25px;
        }
        .auto-style25 {
            height: 23px;
            text-align: center;
        }
        .auto-style5 {
            font-size: xx-large;
        }
        .auto-style26 {
            width: 757px;
            font-size: large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4"><span class="auto-style2">Gerente: </span>
                        <asp:Label ID="LblGerente" runat="server" CssClass="auto-style2" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="auto-style25">
                        <br />
                        <strong>
                        <span class="auto-style5">AGREGAR USUARIO</span></strong><br />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style12">
                        <asp:Label ID="LblIDUsuario" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td class="auto-style14">
                        &nbsp;</td>
                    <td class="auto-style16">&nbsp;</td>
                    <td class="auto-style18">
                        <asp:Button ID="BtnMenuPrincipal" runat="server" Height="35px" Text="Ir a Menu Principal" Width="175px" OnClick="BtnMenuPrincipal_Click" BackColor="#3399FF" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><strong>
                        <asp:Label ID="LblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </strong></td>
                    <td class="auto-style14">
                        &nbsp;</td>
                    <td class="auto-style16">&nbsp;</td>
                    <td class="auto-style19">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style8">Usuario:</td>
                    <td class="auto-style14">
                        <asp:TextBox ID="TxtUsuario" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style26">Contraseña:</td>
                    <td class="auto-style19">
                        <asp:TextBox ID="TxtClave" runat="server" Height="20px" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style14">&nbsp;</td>
                    <td class="auto-style16">&nbsp;</td>
                    <td class="auto-style19">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13"></td>
                    <td class="auto-style15"></td>
                    <td class="auto-style9">&nbsp;</td>
                    <td class="auto-style20">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13"></td>
                    <td class="auto-style15"></td>
                    <td class="auto-style17"></td>
                    <td class="auto-style20"></td>
                </tr>
                <tr>
                    <td class="auto-style21">Rol de Usuario</td>
                    <td class="auto-style22">
                        <asp:DropDownList ID="DdlRol" runat="server" Height="28px" Width="208px">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style23">Asignar a Empleado</td>
                    <td class="auto-style24">
                        <asp:DropDownList ID="DdlEmpleado" runat="server" Height="28px" Width="208px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style13"></td>
                    <td class="auto-style15"></td>
                    <td class="auto-style17"></td>
                    <td class="auto-style20"></td>
                </tr>
                <tr>
                    <td class="auto-style12">&nbsp;</td>
                    <td class="auto-style14">
                        <asp:Button ID="BtnCrear" runat="server" Height="35px" Text="Crear" Width="150px" OnClick="BtnCrear_Click" BackColor="Lime" BorderColor="Black" />
                    </td>
                    <td class="auto-style3" colspan="2">
                        <asp:Button ID="BtnModificar" runat="server" Height="35px" Text="Modificar" Width="150px" OnClick="BtnModificar_Click" ForeColor="Black" BackColor="Lime" BorderColor="Black" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3" colspan="2">
                        &nbsp;</td>
                    <td class="auto-style3" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3" colspan="4">
                        <asp:GridView ID="GvUsuarios" runat="server" OnSelectedIndexChanged="GvUsuarios_SelectedIndexChanged" CellPadding="4" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" Width="920px">
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#333333" />
                            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#487575" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#275353" />
                        </asp:GridView>
                    </td>
                </tr>
                </table>
        </div>
    </form>
</body>
</html>
